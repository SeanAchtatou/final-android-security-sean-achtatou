package tai.haod.rmbd.utils;

import android.app.Activity;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import tai.haod.rmbd.R;

public class HeaderViewHelper {
    private Button mBackBtn = null;
    private ProgressBar mProgBar = null;
    private TextView mTitle = null;

    public HeaderViewHelper(Activity activity) {
        this.mBackBtn = (Button) activity.findViewById(R.id.BackBtn);
        this.mProgBar = (ProgressBar) activity.findViewById(R.id.LoadingProg);
        this.mTitle = (TextView) activity.findViewById(R.id.ViewTitle);
        hideProgress();
    }

    public void setOnClickListener(View.OnClickListener l) {
        this.mBackBtn.setOnClickListener(l);
    }

    public void showProgress() {
        this.mProgBar.setVisibility(0);
    }

    public void hideProgress() {
        this.mProgBar.setVisibility(4);
    }

    public void setTitle(String title) {
        this.mTitle.setText(title);
    }
}
