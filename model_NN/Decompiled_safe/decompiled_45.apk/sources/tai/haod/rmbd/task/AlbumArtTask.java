package tai.haod.rmbd.task;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Shader;
import android.os.AsyncTask;
import tai.haod.rmbd.utils.LastFMHelper;
import tai.haod.rmbd.utils.NetUtils;

public class AlbumArtTask extends AsyncTask<String, Bitmap, Integer> {
    private static float mScale;
    private Listener mListener = null;

    public interface Listener {
        void AAT_OnBegin();

        void AAT_OnFinished(boolean z);

        void AAT_OnReady(Bitmap bitmap);
    }

    public AlbumArtTask(Context ctx, Listener l) {
        this.mListener = l;
        mScale = ctx.getResources().getDisplayMetrics().density;
    }

    /* access modifiers changed from: protected */
    public Integer doInBackground(String... params) {
        Bitmap art;
        Bitmap art2;
        String artist = params[0];
        String albumArtLink = parseLargeAlbumArtLink(NetUtils.loadStringAsPC(LastFMHelper.getAlbumInfoURL(artist, params[1])));
        if (albumArtLink == null || (art2 = NetUtils.loadBitmap(albumArtLink)) == null) {
            String artistLink = parseArtistImageLink(NetUtils.loadStringAsPC(LastFMHelper.getArtistImageURL(artist)));
            if (artistLink == null || (art = NetUtils.loadBitmap(artistLink)) == null) {
                return 0;
            }
            publishProgress(createReflectedImage(art));
            return 1;
        }
        publishProgress(createReflectedImage(art2));
        return 1;
    }

    private static String parseLargeAlbumArtLink(String albumInfo) {
        if (albumInfo == null) {
            return null;
        }
        String link = SearchSongTask.getRangeText(albumInfo, "extralarge\">", "</image>");
        if (link != null) {
            link = link.trim();
        }
        return link;
    }

    private static String parseArtistImageLink(String artistInfo) {
        if (artistInfo == null) {
            return null;
        }
        String block = SearchSongTask.getRangeText(artistInfo, "\"extralarge\"", "/size>");
        if (block == null) {
            return null;
        }
        String link = SearchSongTask.getRangeText(block, ">", "<");
        if (link != null) {
            link = link.trim();
        }
        return link;
    }

    /* JADX INFO: Multiple debug info for r8v8 android.graphics.Paint: [D('reflectionImage' android.graphics.Bitmap), D('paint' android.graphics.Paint)] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void}
     arg types: [int, float, int, float, int, int, android.graphics.Shader$TileMode]
     candidates:
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long, long, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void} */
    private static Bitmap createReflectedImage(Bitmap origin) {
        try {
            Bitmap origin2 = Bitmap.createScaledBitmap(origin, (int) (((float) origin.getWidth()) * mScale), (int) (((float) origin.getHeight()) * mScale), true);
            int width = origin2.getWidth();
            int height = origin2.getHeight();
            Matrix matrix = new Matrix();
            matrix.preScale(1.0f, -1.0f);
            Bitmap reflectionImage = Bitmap.createBitmap(origin2, 0, height / 4, width, height / 4, matrix, false);
            try {
                Bitmap bitmapWithReflection = Bitmap.createBitmap(width, (height / 4) + height, Bitmap.Config.ARGB_8888);
                Canvas canvas = new Canvas(bitmapWithReflection);
                canvas.drawBitmap(origin2, 0.0f, 0.0f, (Paint) null);
                canvas.drawRect(0.0f, (float) height, (float) width, (float) (height + 4), new Paint());
                canvas.drawBitmap(reflectionImage, 0.0f, (float) (height + 4), (Paint) null);
                Paint paint = new Paint();
                paint.setShader(new LinearGradient(0.0f, (float) origin2.getHeight(), 0.0f, (float) (bitmapWithReflection.getHeight() + 4), 1895825407, 16777215, Shader.TileMode.CLAMP));
                paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
                canvas.drawRect(0.0f, (float) height, (float) width, (float) (bitmapWithReflection.getHeight() + 4), paint);
                return bitmapWithReflection;
            } catch (Exception e) {
                return origin2;
            }
        } catch (Exception e2) {
            return origin;
        }
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        if (this.mListener != null) {
            this.mListener.AAT_OnBegin();
        }
    }

    /* access modifiers changed from: protected */
    public void onProgressUpdate(Bitmap... bmps) {
        if (this.mListener != null) {
            this.mListener.AAT_OnReady(bmps[0]);
        }
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Integer result) {
        if (this.mListener != null) {
            this.mListener.AAT_OnFinished(result.intValue() != 1);
        }
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
        this.mListener = null;
    }
}
