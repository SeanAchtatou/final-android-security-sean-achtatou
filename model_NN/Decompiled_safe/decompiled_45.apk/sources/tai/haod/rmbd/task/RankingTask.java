package tai.haod.rmbd.task;

import android.os.AsyncTask;
import org.json.JSONArray;
import org.json.JSONObject;
import tai.haod.rmbd.data.History;
import tai.haod.rmbd.utils.NetUtils;

public class RankingTask extends AsyncTask<String, String, Integer> {
    private Listener mListener = null;

    public interface Listener {
        void RT_OnBegin();

        void RT_OnFinished(boolean z);

        void RT_OnReady(String str, String str2);
    }

    public RankingTask(Listener l) {
        this.mListener = l;
    }

    /* access modifiers changed from: protected */
    public Integer doInBackground(String... urls) {
        String httpResp = NetUtils.loadString(urls[0]);
        if (httpResp == null) {
            return 0;
        }
        try {
            JSONArray items = new JSONArray(httpResp);
            for (int i = 0; i < items.length(); i++) {
                JSONObject item = items.getJSONObject(i);
                publishProgress(item.getString(History.COL_SONG), item.getString("artist"));
            }
            return 1;
        } catch (Exception e) {
            return 0;
        }
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        if (this.mListener != null) {
            this.mListener.RT_OnBegin();
        }
    }

    /* access modifiers changed from: protected */
    public void onProgressUpdate(String... results) {
        if (this.mListener != null) {
            this.mListener.RT_OnReady(results[0], results[1]);
        }
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Integer result) {
        if (this.mListener != null) {
            this.mListener.RT_OnFinished(result.intValue() == 0);
        }
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
        this.mListener = null;
    }
}
