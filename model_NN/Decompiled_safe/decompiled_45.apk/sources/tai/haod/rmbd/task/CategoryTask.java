package tai.haod.rmbd.task;

import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.util.Log;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class CategoryTask extends AsyncTask<Context, Integer, Long> {
    public static boolean bReady = false;
    public static ArrayList<HashMap<String, Object>> mAlbumsList;
    public static ArrayList<HashMap<String, Object>> mArtistsList;
    public static ArrayList<HashMap<String, Object>> mFoldersList;
    private Listener mListener = null;

    public interface Listener {
        void CT_OnBegin();

        void CT_OnCategoryReady();

        void CT_OnEnd(boolean z);
    }

    public CategoryTask(Listener l) {
        this.mListener = l;
    }

    /* access modifiers changed from: protected */
    public Long doInBackground(Context... contexts) {
        Context mContext = contexts[0];
        mArtistsList = new ArrayList<>();
        StringBuilder where = new StringBuilder();
        where.append("artist != ''");
        Cursor artistCursor = mContext.getContentResolver().query(MediaStore.Audio.Artists.EXTERNAL_CONTENT_URI, new String[]{"_id", "artist", "number_of_tracks"}, where.toString(), null, "artist_key");
        if (artistCursor != null && artistCursor.getCount() > 0) {
            artistCursor.moveToFirst();
            while (!artistCursor.isAfterLast()) {
                StringBuilder where1 = new StringBuilder();
                where1.append("title != ''");
                where1.append(" AND is_music=1");
                where1.append(" AND artist=" + DatabaseUtils.sqlEscapeString(artistCursor.getString(artistCursor.getColumnIndex("artist"))));
                where1.append(" AND duration>60000");
                Cursor cur = mContext.getContentResolver().query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, new String[]{"_id", "title"}, where1.toString(), null, null);
                if (cur != null && cur.getCount() > 0) {
                    HashMap<String, Object> localHashMap = new HashMap<>();
                    localHashMap.put("artist", artistCursor.getString(artistCursor.getColumnIndex("artist")));
                    localHashMap.put("count", Integer.valueOf(cur.getCount()));
                    mArtistsList.add(localHashMap);
                }
                artistCursor.moveToNext();
            }
        }
        mAlbumsList = new ArrayList<>();
        StringBuilder where2 = new StringBuilder();
        where2.append("album != ''");
        Cursor albumCursor = mContext.getContentResolver().query(MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI, new String[]{"_id", "album", "numsongs"}, where2.toString(), null, "album_key");
        if (albumCursor != null && albumCursor.getCount() > 0) {
            albumCursor.moveToFirst();
            while (!albumCursor.isAfterLast()) {
                StringBuilder where12 = new StringBuilder();
                where12.append("title != ''");
                where12.append(" AND is_music=1");
                where12.append(" AND album_id=" + albumCursor.getString(albumCursor.getColumnIndex("_id")));
                where12.append(" AND duration>60000");
                Cursor cur2 = mContext.getContentResolver().query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, new String[]{"_id", "title"}, where12.toString(), null, null);
                if (cur2 != null && cur2.getCount() > 0) {
                    HashMap<String, Object> localHashMap2 = new HashMap<>();
                    localHashMap2.put("album", albumCursor.getString(albumCursor.getColumnIndex("album")));
                    localHashMap2.put("count", Integer.valueOf(cur2.getCount()));
                    mAlbumsList.add(localHashMap2);
                }
                albumCursor.moveToNext();
            }
        }
        mFoldersList = new ArrayList<>();
        StringBuilder where3 = new StringBuilder();
        where3.append("title != ''");
        where3.append(" AND is_music=1");
        where3.append(" AND duration>60000");
        Cursor folderCursor = mContext.getContentResolver().query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, new String[]{"_id", "title", "_data"}, where3.toString(), null, null);
        if (folderCursor != null && folderCursor.getCount() > 0) {
            folderCursor.moveToFirst();
            while (!folderCursor.isAfterLast()) {
                boolean newEntry = true;
                String path = folderCursor.getString(folderCursor.getColumnIndex("_data"));
                String path2 = path.substring(0, path.lastIndexOf("/"));
                Log.e("CategoryTask", "the directory of music file: " + path2);
                Iterator<HashMap<String, Object>> it = mFoldersList.iterator();
                while (true) {
                    if (it.hasNext()) {
                        HashMap<String, Object> entry = it.next();
                        if (entry.get("directory").equals(path2)) {
                            entry.put("count", Integer.valueOf(Integer.parseInt(entry.get("count").toString()) + 1));
                            newEntry = false;
                            break;
                        }
                    } else {
                        break;
                    }
                }
                if (newEntry) {
                    HashMap<String, Object> localHashMap3 = new HashMap<>();
                    localHashMap3.put("directory", path2);
                    localHashMap3.put("count", "1");
                    mFoldersList.add(localHashMap3);
                }
                folderCursor.moveToNext();
            }
        }
        bReady = true;
        return 1L;
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        if (this.mListener != null) {
            this.mListener.CT_OnBegin();
        }
    }

    /* access modifiers changed from: protected */
    public void onProgressUpdate(Integer... integers) {
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Long result) {
        if (this.mListener != null) {
            this.mListener.CT_OnEnd(result.longValue() != 1);
        }
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
        this.mListener = null;
    }
}
