package tai.haod.rmbd.task;

import android.os.AsyncTask;
import tai.haod.rmbd.utils.NetUtils;

public class SongLinkTask extends AsyncTask<String, String, Integer> {
    private Listener mListener = null;

    public interface Listener {
        void SLT_OnBegin();

        void SLT_OnEnd(boolean z);

        void SLT_OnReady(String str);
    }

    public SongLinkTask(Listener l) {
        this.mListener = l;
    }

    /* access modifiers changed from: protected */
    public Integer doInBackground(String... reqURLs) {
        String httpResp = NetUtils.loadStringAsPC(reqURLs[0]);
        if (httpResp == null) {
            return 0;
        }
        String infoText = SearchSongTask.getRangeText(httpResp, "linkbox", "</a>");
        if (infoText == null) {
            return 0;
        }
        String url = SearchSongTask.getRangeText(infoText, "href=\"", "\" ");
        if (url.length() < 6) {
            return 0;
        }
        publishProgress(url);
        return 1;
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        if (this.mListener != null) {
            this.mListener.SLT_OnBegin();
        }
    }

    /* access modifiers changed from: protected */
    public void onProgressUpdate(String... urls) {
        if (this.mListener != null) {
            this.mListener.SLT_OnReady(urls[0]);
        }
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Integer result) {
        if (this.mListener != null) {
            this.mListener.SLT_OnEnd(result.intValue() != 1);
        }
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
        this.mListener = null;
    }
}
