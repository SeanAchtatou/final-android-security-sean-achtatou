package tai.haod.rmbd.service;

import android.content.Context;
import android.net.Uri;
import tai.haod.rmbd.data.DownloadQueue;
import tai.haod.rmbd.utils.Constants;

public class DownloadInfo {
    public int mControl;
    public int mCurrentBytes;
    public String mETag;
    public String mFileName;
    public volatile boolean mHasActiveThread;
    public String mHint;
    public int mId;
    public long mLastMod;
    public boolean mMediaScanned;
    public String mMimeType;
    public int mNumFailed;
    public int mRedirectCount;
    public int mRetryAfter;
    public int mStatus;
    public int mTotalBytes;
    public String mUri;

    public DownloadInfo(int id, String uri, String hint, String fileName, String mimeType, int control, int status, int numFailed, int retryAfter, int redirectCount, long lastMod, int totalBytes, int currentBytes, String eTag, boolean mediaScanned) {
        this.mId = id;
        this.mUri = uri;
        this.mHint = hint;
        this.mFileName = fileName;
        this.mMimeType = mimeType;
        this.mControl = control;
        this.mStatus = status;
        this.mNumFailed = numFailed;
        this.mRetryAfter = retryAfter;
        this.mRedirectCount = redirectCount;
        this.mLastMod = lastMod;
        this.mTotalBytes = totalBytes;
        this.mCurrentBytes = currentBytes;
        this.mETag = eTag;
        this.mMediaScanned = mediaScanned;
    }

    public void sendIntentIfRequested(Uri contentUri, Context context) {
    }

    public long restartTime() {
        if (this.mRetryAfter > 0) {
            return this.mLastMod + ((long) this.mRetryAfter);
        }
        return this.mLastMod + ((long) ((DownloadHelper.sRandom.nextInt(1001) + Constants.MAX_DOWNLOADS) * 30 * (1 << (this.mNumFailed - 1))));
    }

    public boolean isReadyToStart(long now) {
        if (this.mControl == 1) {
            return false;
        }
        if (this.mStatus == 0) {
            return true;
        }
        if (this.mStatus == 191) {
            return true;
        }
        if (this.mStatus == 190) {
            return true;
        }
        if (this.mStatus == 193) {
            if (this.mNumFailed == 0) {
                return true;
            }
            if (restartTime() < now) {
                return true;
            }
        }
        return false;
    }

    public boolean isReadyToRestart(long now) {
        if (this.mControl == 1) {
            return false;
        }
        if (this.mStatus == 0) {
            return true;
        }
        if (this.mStatus == 191) {
            return true;
        }
        if (this.mStatus == 193) {
            if (this.mNumFailed == 0) {
                return true;
            }
            if (restartTime() < now) {
                return true;
            }
        }
        return false;
    }

    public boolean hasCompletionNotification() {
        if (!DownloadQueue.isStatusCompleted(this.mStatus)) {
            return false;
        }
        return true;
    }

    public boolean canUseNetwork(boolean available, boolean roaming) {
        if (!available) {
            return false;
        }
        return true;
    }
}
