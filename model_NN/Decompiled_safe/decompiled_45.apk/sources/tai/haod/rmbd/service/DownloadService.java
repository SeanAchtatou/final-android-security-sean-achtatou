package tai.haod.rmbd.service;

import android.app.Service;
import android.content.ComponentName;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.CharArrayBuffer;
import android.database.ContentObserver;
import android.database.Cursor;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import com.qwapi.adclient.android.utils.Utils;
import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import tai.haod.rmbd.data.DownloadQueue;
import tai.haod.rmbd.utils.Constants;

public class DownloadService extends Service {
    /* access modifiers changed from: private */
    public ArrayList<DownloadInfo> mDownloads;
    /* access modifiers changed from: private */
    public MyMediaScannerConnection mMediaScannerConn = null;
    /* access modifiers changed from: private */
    public CharArrayBuffer mNewChars;
    /* access modifiers changed from: private */
    public DownloadNotification mNotifier;
    private DownloadManagerContentObserver mObserver;
    /* access modifiers changed from: private */
    public boolean mPendingUpdate;
    /* access modifiers changed from: private */
    public UpdateThread mUpdateThread;
    /* access modifiers changed from: private */
    public CharArrayBuffer oldChars;

    private class DownloadManagerContentObserver extends ContentObserver {
        public DownloadManagerContentObserver() {
            super(new Handler());
        }

        public void onChange(boolean selfChange) {
            DownloadService.this.updateFromProvider();
        }
    }

    private class MyMediaScannerConnection extends MediaScannerConnection {
        public MyMediaScannerConnection(Context context) {
            super(context, null);
        }

        public void onServiceConnected(ComponentName className, IBinder service) {
            DownloadService.this.updateFromProvider();
        }

        public void onServiceDisconnected(ComponentName className) {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
         arg types: [java.lang.String, int]
         candidates:
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
        public boolean scanFile(Cursor cursor, int arrayPos) {
            DownloadInfo info = (DownloadInfo) DownloadService.this.mDownloads.get(arrayPos);
            synchronized (this) {
                scanFile(info.mFileName, info.mMimeType);
                if (cursor != null) {
                    ContentValues values = new ContentValues();
                    values.put(DownloadQueue.COL_MEDIA_SCANNED, (Integer) 1);
                    DownloadService.this.getContentResolver().update(ContentUris.withAppendedId(DownloadQueue.CONTENT_URI, cursor.getLong(cursor.getColumnIndexOrThrow("_id"))), values, null, null);
                }
            }
            return true;
        }
    }

    public IBinder onBind(Intent i) {
        throw new UnsupportedOperationException("Cannot bind to Download Manager Service");
    }

    public void onCreate() {
        super.onCreate();
        this.mDownloads = new ArrayList<>();
        this.mObserver = new DownloadManagerContentObserver();
        getContentResolver().registerContentObserver(DownloadQueue.CONTENT_URI, true, this.mObserver);
        this.mMediaScannerConn = new MyMediaScannerConnection(this);
        this.mMediaScannerConn.connect();
        this.mNotifier = new DownloadNotification(this);
        this.mNotifier.updateNotification();
        trimDatabase();
        removeSpuriousFiles();
        updateFromProvider();
    }

    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        updateFromProvider();
    }

    public void onDestroy() {
        getContentResolver().unregisterContentObserver(this.mObserver);
        if (this.mMediaScannerConn != null) {
            this.mMediaScannerConn.disconnect();
        }
        super.onDestroy();
    }

    /* access modifiers changed from: private */
    public void updateFromProvider() {
        synchronized (this) {
            this.mPendingUpdate = true;
            if (this.mUpdateThread == null) {
                this.mUpdateThread = new UpdateThread();
                this.mUpdateThread.start();
            }
        }
    }

    private class UpdateThread extends Thread {
        public UpdateThread() {
            super("Download Service");
        }

        /* JADX WARNING: Code restructure failed: missing block: B:101:0x02a1, code lost:
            if (tai.haod.rmbd.service.DownloadService.access$9(r0.this$0).isConnected() == false) goto L_0x02b2;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:103:0x02b0, code lost:
            if (tai.haod.rmbd.service.DownloadService.access$9(r0.this$0).scanFile(r5, r6) != false) goto L_0x02b6;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:104:0x02b2, code lost:
            r20 = true;
            r19 = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:106:0x02bf, code lost:
            if (tai.haod.rmbd.service.DownloadService.access$12(r0.this$0, r6) == false) goto L_0x02c3;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:107:0x02c1, code lost:
            r19 = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:108:0x02c3, code lost:
            r23 = tai.haod.rmbd.service.DownloadService.access$13(r0.this$0, r6, r25);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:109:0x02d4, code lost:
            if (r23 != 0) goto L_0x02e6;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:110:0x02d6, code lost:
            r19 = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:111:0x02d8, code lost:
            r6 = r6 + 1;
            r5.moveToNext();
            r18 = r5.isAfterLast();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:112:0x02e3, code lost:
            r11 = false;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:114:0x02ea, code lost:
            if (r23 <= 0) goto L_0x02d8;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:116:0x02ee, code lost:
            if (r23 >= r28) goto L_0x02d8;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:117:0x02f0, code lost:
            r28 = r23;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:118:0x02f3, code lost:
            r4 = r0.this$0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:119:0x02f9, code lost:
            if (r12 >= 1) goto L_0x0365;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:120:0x02fb, code lost:
            r11 = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:122:0x0307, code lost:
            if (tai.haod.rmbd.service.DownloadService.access$11(r4, r5, r6, r21, r22, r25, r11) == false) goto L_0x030b;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:123:0x0309, code lost:
            r12 = r12 + 1;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:125:0x0314, code lost:
            if (tai.haod.rmbd.service.DownloadService.access$8(r0.this$0, r6) == false) goto L_0x0338;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:127:0x0323, code lost:
            if (tai.haod.rmbd.service.DownloadService.access$9(r0.this$0).isConnected() == false) goto L_0x0334;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:129:0x0332, code lost:
            if (tai.haod.rmbd.service.DownloadService.access$9(r0.this$0).scanFile(r5, r6) != false) goto L_0x0338;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:130:0x0334, code lost:
            r20 = true;
            r19 = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:132:0x0341, code lost:
            if (tai.haod.rmbd.service.DownloadService.access$12(r0.this$0, r6) == false) goto L_0x0345;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:133:0x0343, code lost:
            r19 = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:134:0x0345, code lost:
            r23 = tai.haod.rmbd.service.DownloadService.access$13(r0.this$0, r6, r25);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:135:0x0356, code lost:
            if (r23 != 0) goto L_0x0368;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:136:0x0358, code lost:
            r19 = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:137:0x035a, code lost:
            r6 = r6 + 1;
            r5.moveToNext();
            r18 = r5.isAfterLast();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:138:0x0365, code lost:
            r11 = false;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:140:0x036c, code lost:
            if (r23 <= 0) goto L_0x035a;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:142:0x0370, code lost:
            if (r23 >= r28) goto L_0x035a;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:143:0x0372, code lost:
            r28 = r23;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:144:0x0375, code lost:
            tai.haod.rmbd.service.DownloadService.access$9(r0.this$0).disconnect();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:162:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:29:0x00be, code lost:
            r21 = tai.haod.rmbd.service.DownloadHelper.isNetworkAvailable(r0.this$0);
            r22 = tai.haod.rmbd.service.DownloadHelper.isNetworkRoaming(r0.this$0);
            r25 = java.lang.System.currentTimeMillis();
            r5 = r0.this$0.getContentResolver().query(tai.haod.rmbd.data.DownloadQueue.CONTENT_URI, null, null, null, "_id");
         */
        /* JADX WARNING: Code restructure failed: missing block: B:30:0x00e8, code lost:
            if (r5 == null) goto L_0x007b;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:31:0x00ea, code lost:
            r12 = 0;
            r5.moveToFirst();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:33:0x00f2, code lost:
            if (r5.isAfterLast() == false) goto L_0x014a;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:34:0x00f4, code lost:
            r5.moveToFirst();
            r6 = 0;
            r20 = false;
            r19 = false;
            r28 = Long.MAX_VALUE;
            r18 = r5.isAfterLast();
            r16 = r5.getColumnIndexOrThrow("_id");
         */
        /* JADX WARNING: Code restructure failed: missing block: B:35:0x010b, code lost:
            if (r18 == false) goto L_0x0160;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:37:0x011a, code lost:
            if (r6 < tai.haod.rmbd.service.DownloadService.access$1(r0.this$0).size()) goto L_0x0160;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:38:0x011c, code lost:
            tai.haod.rmbd.service.DownloadService.access$15(r0.this$0).updateNotification();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:39:0x0128, code lost:
            if (r20 == false) goto L_0x0375;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:41:0x0137, code lost:
            if (tai.haod.rmbd.service.DownloadService.access$9(r0.this$0).isConnected() != false) goto L_0x0145;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:42:0x0139, code lost:
            tai.haod.rmbd.service.DownloadService.access$9(r0.this$0).connect();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:45:0x0158, code lost:
            if (tai.haod.rmbd.data.DownloadQueue.isStatusRunning(r5.getInt(r5.getColumnIndex(tai.haod.rmbd.data.DownloadQueue.COL_STATUS))) == false) goto L_0x015c;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:46:0x015a, code lost:
            r12 = r12 + 1;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:47:0x015c, code lost:
            r5.moveToNext();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:48:0x0160, code lost:
            if (r18 == false) goto L_0x0193;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:50:0x016b, code lost:
            if (tai.haod.rmbd.service.DownloadService.access$8(r0.this$0, r6) == false) goto L_0x0189;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:52:0x017a, code lost:
            if (tai.haod.rmbd.service.DownloadService.access$9(r0.this$0).isConnected() == false) goto L_0x0189;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:53:0x017c, code lost:
            tai.haod.rmbd.service.DownloadService.access$9(r0.this$0).scanFile(null, r6);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:54:0x0189, code lost:
            tai.haod.rmbd.service.DownloadService.access$10(r0.this$0, r6);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:55:0x0193, code lost:
            r15 = r5.getInt(r16);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:56:0x01a7, code lost:
            if (r6 != tai.haod.rmbd.service.DownloadService.access$1(r0.this$0).size()) goto L_0x022b;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:57:0x01a9, code lost:
            r4 = r0.this$0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:58:0x01af, code lost:
            if (r12 >= 1) goto L_0x021b;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:59:0x01b1, code lost:
            r11 = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:61:0x01bd, code lost:
            if (tai.haod.rmbd.service.DownloadService.access$11(r4, r5, r6, r21, r22, r25, r11) == false) goto L_0x01c1;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:62:0x01bf, code lost:
            r12 = r12 + 1;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:64:0x01ca, code lost:
            if (tai.haod.rmbd.service.DownloadService.access$8(r0.this$0, r6) == false) goto L_0x01ee;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:66:0x01d9, code lost:
            if (tai.haod.rmbd.service.DownloadService.access$9(r0.this$0).isConnected() == false) goto L_0x01ea;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:68:0x01e8, code lost:
            if (tai.haod.rmbd.service.DownloadService.access$9(r0.this$0).scanFile(r5, r6) != false) goto L_0x01ee;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:69:0x01ea, code lost:
            r20 = true;
            r19 = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:71:0x01f7, code lost:
            if (tai.haod.rmbd.service.DownloadService.access$12(r0.this$0, r6) == false) goto L_0x01fb;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:72:0x01f9, code lost:
            r19 = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:73:0x01fb, code lost:
            r23 = tai.haod.rmbd.service.DownloadService.access$13(r0.this$0, r6, r25);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:74:0x020c, code lost:
            if (r23 != 0) goto L_0x021e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:75:0x020e, code lost:
            r19 = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:76:0x0210, code lost:
            r6 = r6 + 1;
            r5.moveToNext();
            r18 = r5.isAfterLast();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:77:0x021b, code lost:
            r11 = false;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:79:0x0222, code lost:
            if (r23 <= 0) goto L_0x0210;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:81:0x0226, code lost:
            if (r23 >= r28) goto L_0x0210;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:82:0x0228, code lost:
            r28 = r23;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:83:0x022b, code lost:
            r14 = ((tai.haod.rmbd.service.DownloadInfo) tai.haod.rmbd.service.DownloadService.access$1(r0.this$0).get(r6)).mId;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:84:0x023c, code lost:
            if (r14 >= r15) goto L_0x026f;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:86:0x0247, code lost:
            if (tai.haod.rmbd.service.DownloadService.access$8(r0.this$0, r6) == false) goto L_0x0265;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:88:0x0256, code lost:
            if (tai.haod.rmbd.service.DownloadService.access$9(r0.this$0).isConnected() == false) goto L_0x0265;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:89:0x0258, code lost:
            tai.haod.rmbd.service.DownloadService.access$9(r0.this$0).scanFile(null, r6);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:90:0x0265, code lost:
            tai.haod.rmbd.service.DownloadService.access$10(r0.this$0, r6);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:91:0x026f, code lost:
            if (r14 != r15) goto L_0x02f3;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:92:0x0271, code lost:
            r4 = r0.this$0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:93:0x0277, code lost:
            if (r12 >= 1) goto L_0x02e3;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:94:0x0279, code lost:
            r11 = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:96:0x0285, code lost:
            if (tai.haod.rmbd.service.DownloadService.access$14(r4, r5, r6, r21, r22, r25, r11) == false) goto L_0x0289;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:97:0x0287, code lost:
            r12 = r12 + 1;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:99:0x0292, code lost:
            if (tai.haod.rmbd.service.DownloadService.access$8(r0.this$0, r6) == false) goto L_0x02b6;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r30 = this;
                r4 = 10
                android.os.Process.setThreadPriority(r4)
                r19 = 0
                r28 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            L_0x000c:
                r0 = r30
                tai.haod.rmbd.service.DownloadService r0 = tai.haod.rmbd.service.DownloadService.this
                r4 = r0
                monitor-enter(r4)
                r0 = r30
                tai.haod.rmbd.service.DownloadService r0 = tai.haod.rmbd.service.DownloadService.this     // Catch:{ all -> 0x0028 }
                r5 = r0
                tai.haod.rmbd.service.DownloadService$UpdateThread r5 = r5.mUpdateThread     // Catch:{ all -> 0x0028 }
                r0 = r5
                r1 = r30
                if (r0 == r1) goto L_0x002b
                java.lang.IllegalStateException r5 = new java.lang.IllegalStateException     // Catch:{ all -> 0x0028 }
                java.lang.String r6 = "multiple UpdateThreads in DownloadService"
                r5.<init>(r6)     // Catch:{ all -> 0x0028 }
                throw r5     // Catch:{ all -> 0x0028 }
            L_0x0028:
                r5 = move-exception
                monitor-exit(r4)     // Catch:{ all -> 0x0028 }
                throw r5
            L_0x002b:
                r0 = r30
                tai.haod.rmbd.service.DownloadService r0 = tai.haod.rmbd.service.DownloadService.this     // Catch:{ all -> 0x0028 }
                r5 = r0
                boolean r5 = r5.mPendingUpdate     // Catch:{ all -> 0x0028 }
                if (r5 != 0) goto L_0x00b4
                r0 = r30
                tai.haod.rmbd.service.DownloadService r0 = tai.haod.rmbd.service.DownloadService.this     // Catch:{ all -> 0x0028 }
                r5 = r0
                r6 = 0
                r5.mUpdateThread = r6     // Catch:{ all -> 0x0028 }
                if (r19 != 0) goto L_0x0049
                r0 = r30
                tai.haod.rmbd.service.DownloadService r0 = tai.haod.rmbd.service.DownloadService.this     // Catch:{ all -> 0x0028 }
                r5 = r0
                r5.stopSelf()     // Catch:{ all -> 0x0028 }
            L_0x0049:
                r7 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
                int r5 = (r28 > r7 ? 1 : (r28 == r7 ? 0 : -1))
                if (r5 == 0) goto L_0x0068
                r0 = r30
                tai.haod.rmbd.service.DownloadService r0 = tai.haod.rmbd.service.DownloadService.this     // Catch:{ all -> 0x0028 }
                r5 = r0
                java.lang.String r6 = "alarm"
                java.lang.Object r13 = r5.getSystemService(r6)     // Catch:{ all -> 0x0028 }
                android.app.AlarmManager r13 = (android.app.AlarmManager) r13     // Catch:{ all -> 0x0028 }
                if (r13 != 0) goto L_0x007c
                java.lang.String r5 = "alanmusicplayer"
                java.lang.String r6 = "couldn't get alarm manager"
                android.util.Log.e(r5, r6)     // Catch:{ all -> 0x0028 }
            L_0x0068:
                r0 = r30
                tai.haod.rmbd.service.DownloadService r0 = tai.haod.rmbd.service.DownloadService.this     // Catch:{ all -> 0x0028 }
                r5 = r0
                r6 = 0
                r5.oldChars = r6     // Catch:{ all -> 0x0028 }
                r0 = r30
                tai.haod.rmbd.service.DownloadService r0 = tai.haod.rmbd.service.DownloadService.this     // Catch:{ all -> 0x0028 }
                r5 = r0
                r6 = 0
                r5.mNewChars = r6     // Catch:{ all -> 0x0028 }
                monitor-exit(r4)     // Catch:{ all -> 0x0028 }
            L_0x007b:
                return
            L_0x007c:
                android.content.Intent r17 = new android.content.Intent     // Catch:{ all -> 0x0028 }
                java.lang.String r5 = "muzilla.intent.action.DOWNLOAD_WAKEUP"
                r0 = r17
                r1 = r5
                r0.<init>(r1)     // Catch:{ all -> 0x0028 }
                r0 = r30
                tai.haod.rmbd.service.DownloadService r0 = tai.haod.rmbd.service.DownloadService.this     // Catch:{ all -> 0x0028 }
                r5 = r0
                java.lang.Class<tai.haod.rmbd.service.DownloadReceiver> r6 = tai.haod.rmbd.service.DownloadReceiver.class
                java.lang.String r6 = r6.getName()     // Catch:{ all -> 0x0028 }
                r0 = r17
                r1 = r5
                r2 = r6
                r0.setClassName(r1, r2)     // Catch:{ all -> 0x0028 }
                r5 = 0
                long r7 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0028 }
                long r7 = r7 + r28
                r0 = r30
                tai.haod.rmbd.service.DownloadService r0 = tai.haod.rmbd.service.DownloadService.this     // Catch:{ all -> 0x0028 }
                r6 = r0
                r9 = 0
                r10 = 1073741824(0x40000000, float:2.0)
                r0 = r6
                r1 = r9
                r2 = r17
                r3 = r10
                android.app.PendingIntent r6 = android.app.PendingIntent.getBroadcast(r0, r1, r2, r3)     // Catch:{ all -> 0x0028 }
                r13.set(r5, r7, r6)     // Catch:{ all -> 0x0028 }
                goto L_0x0068
            L_0x00b4:
                r0 = r30
                tai.haod.rmbd.service.DownloadService r0 = tai.haod.rmbd.service.DownloadService.this     // Catch:{ all -> 0x0028 }
                r5 = r0
                r6 = 0
                r5.mPendingUpdate = r6     // Catch:{ all -> 0x0028 }
                monitor-exit(r4)     // Catch:{ all -> 0x0028 }
                r0 = r30
                tai.haod.rmbd.service.DownloadService r0 = tai.haod.rmbd.service.DownloadService.this
                r4 = r0
                boolean r21 = tai.haod.rmbd.service.DownloadHelper.isNetworkAvailable(r4)
                r0 = r30
                tai.haod.rmbd.service.DownloadService r0 = tai.haod.rmbd.service.DownloadService.this
                r4 = r0
                boolean r22 = tai.haod.rmbd.service.DownloadHelper.isNetworkRoaming(r4)
                long r25 = java.lang.System.currentTimeMillis()
                r0 = r30
                tai.haod.rmbd.service.DownloadService r0 = tai.haod.rmbd.service.DownloadService.this
                r4 = r0
                android.content.ContentResolver r4 = r4.getContentResolver()
                android.net.Uri r5 = tai.haod.rmbd.data.DownloadQueue.CONTENT_URI
                r6 = 0
                r7 = 0
                r8 = 0
                java.lang.String r9 = "_id"
                android.database.Cursor r5 = r4.query(r5, r6, r7, r8, r9)
                if (r5 == 0) goto L_0x007b
                r12 = 0
                r5.moveToFirst()
            L_0x00ee:
                boolean r4 = r5.isAfterLast()
                if (r4 == 0) goto L_0x014a
                r5.moveToFirst()
                r6 = 0
                r20 = 0
                r19 = 0
                r28 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
                boolean r18 = r5.isAfterLast()
                java.lang.String r4 = "_id"
                int r16 = r5.getColumnIndexOrThrow(r4)
            L_0x010b:
                if (r18 == 0) goto L_0x0160
                r0 = r30
                tai.haod.rmbd.service.DownloadService r0 = tai.haod.rmbd.service.DownloadService.this
                r4 = r0
                java.util.ArrayList r4 = r4.mDownloads
                int r4 = r4.size()
                if (r6 < r4) goto L_0x0160
                r0 = r30
                tai.haod.rmbd.service.DownloadService r0 = tai.haod.rmbd.service.DownloadService.this
                r4 = r0
                tai.haod.rmbd.service.DownloadNotification r4 = r4.mNotifier
                r4.updateNotification()
                if (r20 == 0) goto L_0x0375
                r0 = r30
                tai.haod.rmbd.service.DownloadService r0 = tai.haod.rmbd.service.DownloadService.this
                r4 = r0
                tai.haod.rmbd.service.DownloadService$MyMediaScannerConnection r4 = r4.mMediaScannerConn
                boolean r4 = r4.isConnected()
                if (r4 != 0) goto L_0x0145
                r0 = r30
                tai.haod.rmbd.service.DownloadService r0 = tai.haod.rmbd.service.DownloadService.this
                r4 = r0
                tai.haod.rmbd.service.DownloadService$MyMediaScannerConnection r4 = r4.mMediaScannerConn
                r4.connect()
            L_0x0145:
                r5.close()
                goto L_0x000c
            L_0x014a:
                java.lang.String r4 = "status"
                int r4 = r5.getColumnIndex(r4)
                int r27 = r5.getInt(r4)
                boolean r4 = tai.haod.rmbd.data.DownloadQueue.isStatusRunning(r27)
                if (r4 == 0) goto L_0x015c
                int r12 = r12 + 1
            L_0x015c:
                r5.moveToNext()
                goto L_0x00ee
            L_0x0160:
                if (r18 == 0) goto L_0x0193
                r0 = r30
                tai.haod.rmbd.service.DownloadService r0 = tai.haod.rmbd.service.DownloadService.this
                r4 = r0
                boolean r4 = r4.shouldScanFile(r6)
                if (r4 == 0) goto L_0x0189
                r0 = r30
                tai.haod.rmbd.service.DownloadService r0 = tai.haod.rmbd.service.DownloadService.this
                r4 = r0
                tai.haod.rmbd.service.DownloadService$MyMediaScannerConnection r4 = r4.mMediaScannerConn
                boolean r4 = r4.isConnected()
                if (r4 == 0) goto L_0x0189
                r0 = r30
                tai.haod.rmbd.service.DownloadService r0 = tai.haod.rmbd.service.DownloadService.this
                r4 = r0
                tai.haod.rmbd.service.DownloadService$MyMediaScannerConnection r4 = r4.mMediaScannerConn
                r7 = 0
                r4.scanFile(r7, r6)
            L_0x0189:
                r0 = r30
                tai.haod.rmbd.service.DownloadService r0 = tai.haod.rmbd.service.DownloadService.this
                r4 = r0
                r4.deleteDownload(r6)
                goto L_0x010b
            L_0x0193:
                r0 = r5
                r1 = r16
                int r15 = r0.getInt(r1)
                r0 = r30
                tai.haod.rmbd.service.DownloadService r0 = tai.haod.rmbd.service.DownloadService.this
                r4 = r0
                java.util.ArrayList r4 = r4.mDownloads
                int r4 = r4.size()
                if (r6 != r4) goto L_0x022b
                r0 = r30
                tai.haod.rmbd.service.DownloadService r0 = tai.haod.rmbd.service.DownloadService.this
                r4 = r0
                r7 = 1
                if (r12 >= r7) goto L_0x021b
                r7 = 1
                r11 = r7
            L_0x01b3:
                r7 = r21
                r8 = r22
                r9 = r25
                boolean r4 = r4.insertDownload(r5, r6, r7, r8, r9, r11)
                if (r4 == 0) goto L_0x01c1
                int r12 = r12 + 1
            L_0x01c1:
                r0 = r30
                tai.haod.rmbd.service.DownloadService r0 = tai.haod.rmbd.service.DownloadService.this
                r4 = r0
                boolean r4 = r4.shouldScanFile(r6)
                if (r4 == 0) goto L_0x01ee
                r0 = r30
                tai.haod.rmbd.service.DownloadService r0 = tai.haod.rmbd.service.DownloadService.this
                r4 = r0
                tai.haod.rmbd.service.DownloadService$MyMediaScannerConnection r4 = r4.mMediaScannerConn
                boolean r4 = r4.isConnected()
                if (r4 == 0) goto L_0x01ea
                r0 = r30
                tai.haod.rmbd.service.DownloadService r0 = tai.haod.rmbd.service.DownloadService.this
                r4 = r0
                tai.haod.rmbd.service.DownloadService$MyMediaScannerConnection r4 = r4.mMediaScannerConn
                boolean r4 = r4.scanFile(r5, r6)
                if (r4 != 0) goto L_0x01ee
            L_0x01ea:
                r20 = 1
                r19 = 1
            L_0x01ee:
                r0 = r30
                tai.haod.rmbd.service.DownloadService r0 = tai.haod.rmbd.service.DownloadService.this
                r4 = r0
                boolean r4 = r4.visibleNotification(r6)
                if (r4 == 0) goto L_0x01fb
                r19 = 1
            L_0x01fb:
                r0 = r30
                tai.haod.rmbd.service.DownloadService r0 = tai.haod.rmbd.service.DownloadService.this
                r4 = r0
                r0 = r4
                r1 = r6
                r2 = r25
                long r23 = r0.nextAction(r1, r2)
                r7 = 0
                int r4 = (r23 > r7 ? 1 : (r23 == r7 ? 0 : -1))
                if (r4 != 0) goto L_0x021e
                r19 = 1
            L_0x0210:
                int r6 = r6 + 1
                r5.moveToNext()
                boolean r18 = r5.isAfterLast()
                goto L_0x010b
            L_0x021b:
                r7 = 0
                r11 = r7
                goto L_0x01b3
            L_0x021e:
                r7 = 0
                int r4 = (r23 > r7 ? 1 : (r23 == r7 ? 0 : -1))
                if (r4 <= 0) goto L_0x0210
                int r4 = (r23 > r28 ? 1 : (r23 == r28 ? 0 : -1))
                if (r4 >= 0) goto L_0x0210
                r28 = r23
                goto L_0x0210
            L_0x022b:
                r0 = r30
                tai.haod.rmbd.service.DownloadService r0 = tai.haod.rmbd.service.DownloadService.this
                r4 = r0
                java.util.ArrayList r4 = r4.mDownloads
                java.lang.Object r4 = r4.get(r6)
                tai.haod.rmbd.service.DownloadInfo r4 = (tai.haod.rmbd.service.DownloadInfo) r4
                int r14 = r4.mId
                if (r14 >= r15) goto L_0x026f
                r0 = r30
                tai.haod.rmbd.service.DownloadService r0 = tai.haod.rmbd.service.DownloadService.this
                r4 = r0
                boolean r4 = r4.shouldScanFile(r6)
                if (r4 == 0) goto L_0x0265
                r0 = r30
                tai.haod.rmbd.service.DownloadService r0 = tai.haod.rmbd.service.DownloadService.this
                r4 = r0
                tai.haod.rmbd.service.DownloadService$MyMediaScannerConnection r4 = r4.mMediaScannerConn
                boolean r4 = r4.isConnected()
                if (r4 == 0) goto L_0x0265
                r0 = r30
                tai.haod.rmbd.service.DownloadService r0 = tai.haod.rmbd.service.DownloadService.this
                r4 = r0
                tai.haod.rmbd.service.DownloadService$MyMediaScannerConnection r4 = r4.mMediaScannerConn
                r7 = 0
                r4.scanFile(r7, r6)
            L_0x0265:
                r0 = r30
                tai.haod.rmbd.service.DownloadService r0 = tai.haod.rmbd.service.DownloadService.this
                r4 = r0
                r4.deleteDownload(r6)
                goto L_0x010b
            L_0x026f:
                if (r14 != r15) goto L_0x02f3
                r0 = r30
                tai.haod.rmbd.service.DownloadService r0 = tai.haod.rmbd.service.DownloadService.this
                r4 = r0
                r7 = 1
                if (r12 >= r7) goto L_0x02e3
                r7 = 1
                r11 = r7
            L_0x027b:
                r7 = r21
                r8 = r22
                r9 = r25
                boolean r4 = r4.updateDownload(r5, r6, r7, r8, r9, r11)
                if (r4 == 0) goto L_0x0289
                int r12 = r12 + 1
            L_0x0289:
                r0 = r30
                tai.haod.rmbd.service.DownloadService r0 = tai.haod.rmbd.service.DownloadService.this
                r4 = r0
                boolean r4 = r4.shouldScanFile(r6)
                if (r4 == 0) goto L_0x02b6
                r0 = r30
                tai.haod.rmbd.service.DownloadService r0 = tai.haod.rmbd.service.DownloadService.this
                r4 = r0
                tai.haod.rmbd.service.DownloadService$MyMediaScannerConnection r4 = r4.mMediaScannerConn
                boolean r4 = r4.isConnected()
                if (r4 == 0) goto L_0x02b2
                r0 = r30
                tai.haod.rmbd.service.DownloadService r0 = tai.haod.rmbd.service.DownloadService.this
                r4 = r0
                tai.haod.rmbd.service.DownloadService$MyMediaScannerConnection r4 = r4.mMediaScannerConn
                boolean r4 = r4.scanFile(r5, r6)
                if (r4 != 0) goto L_0x02b6
            L_0x02b2:
                r20 = 1
                r19 = 1
            L_0x02b6:
                r0 = r30
                tai.haod.rmbd.service.DownloadService r0 = tai.haod.rmbd.service.DownloadService.this
                r4 = r0
                boolean r4 = r4.visibleNotification(r6)
                if (r4 == 0) goto L_0x02c3
                r19 = 1
            L_0x02c3:
                r0 = r30
                tai.haod.rmbd.service.DownloadService r0 = tai.haod.rmbd.service.DownloadService.this
                r4 = r0
                r0 = r4
                r1 = r6
                r2 = r25
                long r23 = r0.nextAction(r1, r2)
                r7 = 0
                int r4 = (r23 > r7 ? 1 : (r23 == r7 ? 0 : -1))
                if (r4 != 0) goto L_0x02e6
                r19 = 1
            L_0x02d8:
                int r6 = r6 + 1
                r5.moveToNext()
                boolean r18 = r5.isAfterLast()
                goto L_0x010b
            L_0x02e3:
                r7 = 0
                r11 = r7
                goto L_0x027b
            L_0x02e6:
                r7 = 0
                int r4 = (r23 > r7 ? 1 : (r23 == r7 ? 0 : -1))
                if (r4 <= 0) goto L_0x02d8
                int r4 = (r23 > r28 ? 1 : (r23 == r28 ? 0 : -1))
                if (r4 >= 0) goto L_0x02d8
                r28 = r23
                goto L_0x02d8
            L_0x02f3:
                r0 = r30
                tai.haod.rmbd.service.DownloadService r0 = tai.haod.rmbd.service.DownloadService.this
                r4 = r0
                r7 = 1
                if (r12 >= r7) goto L_0x0365
                r7 = 1
                r11 = r7
            L_0x02fd:
                r7 = r21
                r8 = r22
                r9 = r25
                boolean r4 = r4.insertDownload(r5, r6, r7, r8, r9, r11)
                if (r4 == 0) goto L_0x030b
                int r12 = r12 + 1
            L_0x030b:
                r0 = r30
                tai.haod.rmbd.service.DownloadService r0 = tai.haod.rmbd.service.DownloadService.this
                r4 = r0
                boolean r4 = r4.shouldScanFile(r6)
                if (r4 == 0) goto L_0x0338
                r0 = r30
                tai.haod.rmbd.service.DownloadService r0 = tai.haod.rmbd.service.DownloadService.this
                r4 = r0
                tai.haod.rmbd.service.DownloadService$MyMediaScannerConnection r4 = r4.mMediaScannerConn
                boolean r4 = r4.isConnected()
                if (r4 == 0) goto L_0x0334
                r0 = r30
                tai.haod.rmbd.service.DownloadService r0 = tai.haod.rmbd.service.DownloadService.this
                r4 = r0
                tai.haod.rmbd.service.DownloadService$MyMediaScannerConnection r4 = r4.mMediaScannerConn
                boolean r4 = r4.scanFile(r5, r6)
                if (r4 != 0) goto L_0x0338
            L_0x0334:
                r20 = 1
                r19 = 1
            L_0x0338:
                r0 = r30
                tai.haod.rmbd.service.DownloadService r0 = tai.haod.rmbd.service.DownloadService.this
                r4 = r0
                boolean r4 = r4.visibleNotification(r6)
                if (r4 == 0) goto L_0x0345
                r19 = 1
            L_0x0345:
                r0 = r30
                tai.haod.rmbd.service.DownloadService r0 = tai.haod.rmbd.service.DownloadService.this
                r4 = r0
                r0 = r4
                r1 = r6
                r2 = r25
                long r23 = r0.nextAction(r1, r2)
                r7 = 0
                int r4 = (r23 > r7 ? 1 : (r23 == r7 ? 0 : -1))
                if (r4 != 0) goto L_0x0368
                r19 = 1
            L_0x035a:
                int r6 = r6 + 1
                r5.moveToNext()
                boolean r18 = r5.isAfterLast()
                goto L_0x010b
            L_0x0365:
                r7 = 0
                r11 = r7
                goto L_0x02fd
            L_0x0368:
                r7 = 0
                int r4 = (r23 > r7 ? 1 : (r23 == r7 ? 0 : -1))
                if (r4 <= 0) goto L_0x035a
                int r4 = (r23 > r28 ? 1 : (r23 == r28 ? 0 : -1))
                if (r4 >= 0) goto L_0x035a
                r28 = r23
                goto L_0x035a
            L_0x0375:
                r0 = r30
                tai.haod.rmbd.service.DownloadService r0 = tai.haod.rmbd.service.DownloadService.this
                r4 = r0
                tai.haod.rmbd.service.DownloadService$MyMediaScannerConnection r4 = r4.mMediaScannerConn
                r4.disconnect()
                goto L_0x0145
            */
            throw new UnsupportedOperationException("Method not decompiled: tai.haod.rmbd.service.DownloadService.UpdateThread.run():void");
        }
    }

    private void removeSpuriousFiles() {
        File[] files = Environment.getDownloadCacheDirectory().listFiles();
        if (files != null) {
            HashSet<String> fileSet = new HashSet<>();
            for (int i = 0; i < files.length; i++) {
                if (!files[i].getName().equals(Constants.KNOWN_SPURIOUS_FILENAME)) {
                    fileSet.add(files[i].getPath());
                }
            }
            Cursor cursor = getContentResolver().query(DownloadQueue.CONTENT_URI, new String[]{"_data"}, null, null, null);
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    do {
                        fileSet.remove(cursor.getString(0));
                    } while (cursor.moveToNext());
                }
                cursor.close();
            }
            Iterator<String> iterator = fileSet.iterator();
            while (iterator.hasNext()) {
                new File((String) iterator.next()).delete();
            }
        }
    }

    private void trimDatabase() {
    }

    /* access modifiers changed from: private */
    public boolean insertDownload(Cursor cursor, int arrayPos, boolean networkAvailable, boolean networkRoaming, long now, boolean bShouldStart) {
        boolean z;
        int statusColumn = cursor.getColumnIndexOrThrow(DownloadQueue.COL_STATUS);
        int failedColumn = cursor.getColumnIndexOrThrow(DownloadQueue.FAILED_CONNECTIONS);
        int retryRedirect = cursor.getInt(cursor.getColumnIndexOrThrow(DownloadQueue.RETRY_AFTER_X_REDIRECT_COUNT));
        int i = cursor.getInt(cursor.getColumnIndexOrThrow("_id"));
        String string = cursor.getString(cursor.getColumnIndexOrThrow("uri"));
        String string2 = cursor.getString(cursor.getColumnIndexOrThrow(DownloadQueue.COL_FILE_NAME_HINT));
        String string3 = cursor.getString(cursor.getColumnIndexOrThrow("_data"));
        String string4 = cursor.getString(cursor.getColumnIndexOrThrow("mimetype"));
        int i2 = cursor.getInt(cursor.getColumnIndexOrThrow(DownloadQueue.COL_CONTROL));
        int i3 = cursor.getInt(statusColumn);
        int i4 = cursor.getInt(failedColumn);
        int i5 = 268435455 & retryRedirect;
        int i6 = retryRedirect >> 28;
        long j = cursor.getLong(cursor.getColumnIndexOrThrow("lastmod"));
        int i7 = cursor.getInt(cursor.getColumnIndexOrThrow(DownloadQueue.COL_TOTAL_BYTES));
        int i8 = cursor.getInt(cursor.getColumnIndexOrThrow(DownloadQueue.COL_CURRENT_BYTES));
        String string5 = cursor.getString(cursor.getColumnIndexOrThrow(DownloadQueue.ETAG));
        if (cursor.getInt(cursor.getColumnIndexOrThrow(DownloadQueue.COL_MEDIA_SCANNED)) == 1) {
            z = true;
        } else {
            z = false;
        }
        DownloadInfo info = new DownloadInfo(i, string, string2, string3, string4, i2, i3, i4, i5, i6, j, i7, i8, string5, z);
        this.mDownloads.add(arrayPos, info);
        if (info.mStatus == 0 && info.mMimeType != null) {
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setDataAndType(Uri.fromParts("file", Utils.EMPTY_STRING, null), info.mMimeType);
            if (getPackageManager().resolveActivity(intent, 65536) == null) {
                Log.d(Constants.TAG, "no application to handle MIME type " + info.mMimeType);
                info.mStatus = DownloadQueue.STATUS_NOT_ACCEPTABLE;
                Uri uri = ContentUris.withAppendedId(DownloadQueue.CONTENT_URI, (long) info.mId);
                ContentValues values = new ContentValues();
                values.put(DownloadQueue.COL_STATUS, Integer.valueOf((int) DownloadQueue.STATUS_NOT_ACCEPTABLE));
                getContentResolver().update(uri, values, null, null);
                info.sendIntentIfRequested(uri, this);
                return false;
            }
        }
        if (!info.canUseNetwork(networkAvailable, networkRoaming) || !bShouldStart) {
            if (info.mStatus == 0 || info.mStatus == 191 || info.mStatus == 190) {
                info.mStatus = DownloadQueue.STATUS_RUNNING_PAUSED;
                Uri uri2 = ContentUris.withAppendedId(DownloadQueue.CONTENT_URI, (long) info.mId);
                ContentValues values2 = new ContentValues();
                values2.put(DownloadQueue.COL_STATUS, Integer.valueOf((int) DownloadQueue.STATUS_RUNNING_PAUSED));
                getContentResolver().update(uri2, values2, null, null);
                return true;
            }
        } else if (info.isReadyToStart(now)) {
            if (info.mHasActiveThread) {
                throw new IllegalStateException("Multiple threads on same download on insert");
            }
            if (info.mStatus != 190) {
                info.mStatus = DownloadQueue.STATUS_RUNNING;
                ContentValues values3 = new ContentValues();
                values3.put(DownloadQueue.COL_STATUS, Integer.valueOf(info.mStatus));
                getContentResolver().update(ContentUris.withAppendedId(DownloadQueue.CONTENT_URI, (long) info.mId), values3, null, null);
            }
            DownloadThread downloadThread = new DownloadThread(this, info);
            info.mHasActiveThread = true;
            downloadThread.start();
        }
        return false;
    }

    /* access modifiers changed from: private */
    public boolean updateDownload(Cursor cursor, int arrayPos, boolean networkAvailable, boolean networkRoaming, long now, boolean bShouldStart) {
        DownloadInfo info = this.mDownloads.get(arrayPos);
        int statusColumn = cursor.getColumnIndexOrThrow(DownloadQueue.COL_STATUS);
        int failedColumn = cursor.getColumnIndexOrThrow(DownloadQueue.FAILED_CONNECTIONS);
        info.mId = cursor.getInt(cursor.getColumnIndexOrThrow("_id"));
        info.mUri = stringFromCursor(info.mUri, cursor, "uri");
        info.mHint = stringFromCursor(info.mHint, cursor, DownloadQueue.COL_FILE_NAME_HINT);
        info.mFileName = stringFromCursor(info.mFileName, cursor, "_data");
        info.mMimeType = stringFromCursor(info.mMimeType, cursor, "mimetype");
        synchronized (info) {
            info.mControl = cursor.getInt(cursor.getColumnIndexOrThrow(DownloadQueue.COL_CONTROL));
        }
        if (info.mControl != 0) {
            this.mNotifier.mNotificationMgr.cancel(info.mId);
        }
        info.mStatus = cursor.getInt(statusColumn);
        info.mNumFailed = cursor.getInt(failedColumn);
        int retryRedirect = cursor.getInt(cursor.getColumnIndexOrThrow(DownloadQueue.RETRY_AFTER_X_REDIRECT_COUNT));
        info.mRetryAfter = 268435455 & retryRedirect;
        info.mRedirectCount = retryRedirect >> 28;
        info.mLastMod = cursor.getLong(cursor.getColumnIndexOrThrow("lastmod"));
        info.mTotalBytes = cursor.getInt(cursor.getColumnIndexOrThrow(DownloadQueue.COL_TOTAL_BYTES));
        info.mCurrentBytes = cursor.getInt(cursor.getColumnIndexOrThrow(DownloadQueue.COL_CURRENT_BYTES));
        info.mETag = stringFromCursor(info.mETag, cursor, DownloadQueue.ETAG);
        info.mMediaScanned = cursor.getInt(cursor.getColumnIndexOrThrow(DownloadQueue.COL_MEDIA_SCANNED)) == 1;
        if (!info.canUseNetwork(networkAvailable, networkRoaming) || !bShouldStart || !info.isReadyToRestart(now)) {
            return false;
        }
        if (info.mHasActiveThread) {
            throw new IllegalStateException("Multiple threads on same download on update");
        }
        info.mStatus = DownloadQueue.STATUS_RUNNING;
        ContentValues values = new ContentValues();
        values.put(DownloadQueue.COL_STATUS, Integer.valueOf(info.mStatus));
        getContentResolver().update(ContentUris.withAppendedId(DownloadQueue.CONTENT_URI, (long) info.mId), values, null, null);
        DownloadThread downloader = new DownloadThread(this, info);
        info.mHasActiveThread = true;
        downloader.start();
        return true;
    }

    private String stringFromCursor(String old, Cursor cursor, String column) {
        int index = cursor.getColumnIndexOrThrow(column);
        if (old == null) {
            return cursor.getString(index);
        }
        if (this.mNewChars == null) {
            this.mNewChars = new CharArrayBuffer(128);
        }
        cursor.copyStringToBuffer(index, this.mNewChars);
        int length = this.mNewChars.sizeCopied;
        if (length != old.length()) {
            return cursor.getString(index);
        }
        if (this.oldChars == null || this.oldChars.sizeCopied < length) {
            this.oldChars = new CharArrayBuffer(length);
        }
        char[] oldArray = this.oldChars.data;
        char[] newArray = this.mNewChars.data;
        old.getChars(0, length, oldArray, 0);
        for (int i = length - 1; i >= 0; i--) {
            if (oldArray[i] != newArray[i]) {
                return new String(newArray, 0, length);
            }
        }
        return old;
    }

    /* access modifiers changed from: private */
    public void deleteDownload(int arrayPos) {
        DownloadInfo info = this.mDownloads.get(arrayPos);
        if (info.mStatus == 190) {
            info.mStatus = DownloadQueue.STATUS_CANCELED;
        }
        this.mNotifier.mNotificationMgr.cancel(info.mId);
        this.mDownloads.remove(arrayPos);
    }

    /* access modifiers changed from: private */
    public long nextAction(int arrayPos, long now) {
        DownloadInfo info = this.mDownloads.get(arrayPos);
        if (DownloadQueue.isStatusCompleted(info.mStatus)) {
            return -1;
        }
        if (info.mStatus != 193) {
            return 0;
        }
        if (info.mNumFailed == 0) {
            return 0;
        }
        long when = info.restartTime();
        if (when <= now) {
            return 0;
        }
        return when - now;
    }

    /* access modifiers changed from: private */
    public boolean visibleNotification(int arrayPos) {
        return this.mDownloads.get(arrayPos).hasCompletionNotification();
    }

    /* access modifiers changed from: private */
    public boolean shouldScanFile(int arrayPos) {
        return false;
    }
}
