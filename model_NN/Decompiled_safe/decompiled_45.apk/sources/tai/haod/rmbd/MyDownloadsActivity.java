package tai.haod.rmbd;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.content.ActivityNotFoundException;
import android.content.AsyncQueryHandler;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.qwapi.adclient.android.utils.Utils;
import java.io.File;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.HashMap;
import java.util.Locale;
import tai.haod.rmbd.data.Downloaded;
import tai.haod.rmbd.editor.RingdroidEditActivity;
import tai.haod.rmbd.utils.Constants;
import tai.haod.rmbd.utils.Ringtone;

public class MyDownloadsActivity extends ListActivity {
    private static final int DIALOG_DELETE_YESNO = 2;
    private static final int DIALOG_RING_PICKER = 3;
    private static final int MENU_STANDARD = 1;
    /* access modifiers changed from: private */
    public static Context mContext;
    /* access modifiers changed from: private */
    public static ArrayList<HashMap<String, Object>> mIDMappingList;
    /* access modifiers changed from: private */
    public static String[] mediaCols = {"_id", "title", "title_key", "_data", "album", "artist", "artist_id", "duration"};
    /* access modifiers changed from: private */
    public MyDownloadsAdapter mAdapter = null;
    private boolean mAdapterSent = false;
    /* access modifiers changed from: private */
    public Cursor mCursor = null;
    /* access modifiers changed from: private */
    public String mFilePath = Utils.EMPTY_STRING;
    /* access modifiers changed from: private */
    public Handler mReScanHandler = new Handler() {
        public void handleMessage(Message msg) {
            try {
                Cursor unused = MyDownloadsActivity.this.getMyDownloadsCursor(MyDownloadsActivity.this.mAdapter.getQueryHandler());
            } catch (Exception e) {
            }
        }
    };
    private BroadcastReceiver mScanListener = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            MyDownloadsActivity.this.mReScanHandler.sendEmptyMessage(0);
        }
    };
    /* access modifiers changed from: private */
    public int mSelectedItem = -1;
    /* access modifiers changed from: private */
    public int mSelectedItemMediaID = -1;
    /* access modifiers changed from: private */
    public int ring_button_type;
    String title = null;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.mydownloads);
        mContext = this;
        AdsView.createAdWhirl(this);
        IntentFilter f = new IntentFilter();
        f.addAction("android.intent.action.MEDIA_UNMOUNTED");
        f.addDataScheme("file");
        registerReceiver(this.mScanListener, f);
        this.mAdapter = (MyDownloadsAdapter) getLastNonConfigurationInstance();
        if (this.mAdapter == null) {
            this.mAdapter = new MyDownloadsAdapter(getApplication(), this, R.layout.songs_row, this.mCursor, new String[0], new int[0]);
            setListAdapter(this.mAdapter);
            getMyDownloadsCursor(this.mAdapter.getQueryHandler());
        } else {
            this.mAdapter.setActivity(this);
            setListAdapter(this.mAdapter);
            this.mCursor = this.mAdapter.getCursor();
            if (this.mCursor != null) {
                init(this.mCursor);
            } else {
                getMyDownloadsCursor(this.mAdapter.getQueryHandler());
            }
        }
        mIDMappingList = new ArrayList<>();
        new IDMappingTask(this, null).execute(new Void[0]);
    }

    private class IDMappingTask extends AsyncTask<Void, Void, Void> {
        private IDMappingTask() {
        }

        /* synthetic */ IDMappingTask(MyDownloadsActivity myDownloadsActivity, IDMappingTask iDMappingTask) {
            this();
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(Void... params) {
            Cursor downloadsCursor = MyDownloadsActivity.this.getMyDownloadsCursor(null);
            if (downloadsCursor == null) {
                return null;
            }
            downloadsCursor.moveToFirst();
            while (!downloadsCursor.isAfterLast()) {
                StringBuilder where = new StringBuilder();
                where.append("title != ''");
                where.append(" AND is_music=1");
                where.append(" AND _data=" + DatabaseUtils.sqlEscapeString(downloadsCursor.getString(downloadsCursor.getColumnIndex("_data"))));
                Cursor mediaCursor = ((Activity) MyDownloadsActivity.mContext).managedQuery(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, MyDownloadsActivity.mediaCols, where.toString(), null, null);
                if (mediaCursor != null && mediaCursor.getCount() > 0) {
                    mediaCursor.moveToFirst();
                    long mediaID = mediaCursor.getLong(mediaCursor.getColumnIndex("_id"));
                    int secs = mediaCursor.getInt(mediaCursor.getColumnIndex("duration")) / Constants.MAX_DOWNLOADS;
                    HashMap<String, Object> localHashMap = new HashMap<>();
                    localHashMap.put("downloads_id", String.valueOf(downloadsCursor.getInt(downloadsCursor.getColumnIndex("_id"))));
                    localHashMap.put("media_id", String.valueOf(mediaID));
                    localHashMap.put("duration", String.valueOf(secs));
                    MyDownloadsActivity.mIDMappingList.add(localHashMap);
                }
                downloadsCursor.moveToNext();
            }
            return null;
        }
    }

    public Object onRetainNonConfigurationInstance() {
        this.mAdapterSent = true;
        return this.mAdapter;
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        Cursor c;
        unregisterReceiver(this.mScanListener);
        if (!this.mAdapterSent && (c = this.mAdapter.getCursor()) != null) {
            c.close();
        }
        setListAdapter(null);
        this.mAdapter = null;
        super.onDestroy();
    }

    public void onPause() {
        this.mReScanHandler.removeCallbacksAndMessages(null);
        super.onPause();
    }

    /* access modifiers changed from: private */
    public void init(Cursor c) {
        if (this.mAdapter != null) {
            this.mAdapter.changeCursor(c);
            if (this.mCursor == null) {
                this.mReScanHandler.sendEmptyMessageDelayed(0, 1000);
            }
        }
    }

    /* access modifiers changed from: private */
    public Cursor getMyDownloadsCursor(AsyncQueryHandler async) {
        String[] cols = {"_id", "_data", "title", "artist", "lyric", "album", "lastmod"};
        if (async != null) {
            async.startQuery(0, null, Downloaded.CONTENT_URI, cols, null, null, "lastmod DESC");
            return null;
        }
        try {
            ContentResolver resolver = getContentResolver();
            if (resolver != null) {
                return resolver.query(Downloaded.CONTENT_URI, cols, null, null, "lastmod DESC");
            }
            return null;
        } catch (UnsupportedOperationException e) {
            e.printStackTrace();
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int pos, long id) {
        Cursor c = (Cursor) this.mAdapter.getItem(pos);
        if (c != null) {
            this.mSelectedItem = c.getInt(c.getColumnIndex("_id"));
            this.mFilePath = c.getString(c.getColumnIndex("_data"));
            this.mSelectedItemMediaID = -1;
            if (mIDMappingList.size() > 0) {
                for (int i = 0; i < mIDMappingList.size(); i++) {
                    HashMap<String, Object> localHashMap = mIDMappingList.get(i);
                    if (Integer.parseInt((String) localHashMap.get("downloads_id")) == this.mSelectedItem) {
                        this.mSelectedItemMediaID = Integer.parseInt((String) localHashMap.get("media_id"));
                    }
                }
            }
            showDialog(1);
        }
    }

    private static class MyDownloadsAdapter extends SimpleCursorAdapter {
        private static StringBuilder sFormatBuilder = new StringBuilder();
        private static Formatter sFormatter = new Formatter(sFormatBuilder, Locale.getDefault());
        private static final Object[] sTimeArgs = new Object[5];
        /* access modifiers changed from: private */
        public MyDownloadsActivity mActivity;
        private int mAlbumIdx;
        private int mArtistIdx;
        private AsyncQueryHandler mQueryHandler;
        private int mSizeIdx;
        private int mTitleIdx;

        static class ViewHolder {
            TextView mTVAlbum;
            TextView mTVArtist;
            TextView mTVDuration;
            TextView mTVNo;
            TextView mTVTitle;

            ViewHolder() {
            }
        }

        class QueryHandler extends AsyncQueryHandler {
            public QueryHandler(ContentResolver cr) {
                super(cr);
            }

            /* access modifiers changed from: protected */
            public void onQueryComplete(int token, Object cookie, Cursor cursor) {
                MyDownloadsAdapter.this.mActivity.init(cursor);
            }
        }

        MyDownloadsAdapter(Context context, MyDownloadsActivity currentActivity, int layout, Cursor cursor, String[] from, int[] to) {
            super(context, layout, cursor, from, to);
            this.mActivity = currentActivity;
            this.mQueryHandler = new QueryHandler(context.getContentResolver());
            getColumnIndices(cursor);
        }

        private void getColumnIndices(Cursor c) {
            if (c != null) {
                this.mTitleIdx = c.getColumnIndex("title");
                this.mArtistIdx = c.getColumnIndex("artist");
                this.mAlbumIdx = c.getColumnIndex("album");
            }
        }

        public void setActivity(MyDownloadsActivity newActivity) {
            this.mActivity = newActivity;
        }

        public AsyncQueryHandler getQueryHandler() {
            return this.mQueryHandler;
        }

        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            View v = super.newView(context, cursor, parent);
            ViewHolder vh = new ViewHolder();
            vh.mTVTitle = (TextView) v.findViewById(R.id.tv_song_name);
            vh.mTVArtist = (TextView) v.findViewById(R.id.tv_artist_name);
            vh.mTVAlbum = (TextView) v.findViewById(R.id.tv_album_name);
            vh.mTVDuration = (TextView) v.findViewById(R.id.tv_length);
            v.setTag(vh);
            return v;
        }

        public void bindView(View view, Context context, Cursor c) {
            ViewHolder vh = (ViewHolder) view.getTag();
            vh.mTVTitle.setText(c.getString(this.mTitleIdx));
            vh.mTVArtist.setText(c.getString(this.mArtistIdx));
            vh.mTVAlbum.setText(c.getString(this.mAlbumIdx));
            if (MyDownloadsActivity.mIDMappingList.size() > 0) {
                for (int i = 0; i < MyDownloadsActivity.mIDMappingList.size(); i++) {
                    HashMap<String, Object> localHashMap = (HashMap) MyDownloadsActivity.mIDMappingList.get(i);
                    if (Integer.parseInt((String) localHashMap.get("downloads_id")) == c.getInt(c.getColumnIndex("_id"))) {
                        int secs = Integer.parseInt((String) localHashMap.get("duration"));
                        String durationformat = MyDownloadsActivity.mContext.getString(secs < 3600 ? R.string.durationformatshort : R.string.durationformatlong);
                        sFormatBuilder.setLength(0);
                        Object[] timeArgs = sTimeArgs;
                        timeArgs[0] = Integer.valueOf(secs / 3600);
                        timeArgs[1] = Integer.valueOf(secs / 60);
                        timeArgs[2] = Integer.valueOf((secs / 60) % 60);
                        timeArgs[3] = Integer.valueOf(secs);
                        timeArgs[4] = Integer.valueOf(secs % 60);
                        vh.mTVDuration.setText(sFormatter.format(durationformat, timeArgs).toString());
                    }
                }
            }
        }

        public void changeCursor(Cursor cursor) {
            if (cursor != this.mActivity.mCursor) {
                this.mActivity.mCursor = cursor;
                getColumnIndices(cursor);
                super.changeCursor(cursor);
            }
        }

        public Cursor runQueryOnBackgroundThread(CharSequence constraint) {
            return this.mActivity.getMyDownloadsCursor(null);
        }
    }

    /* access modifiers changed from: private */
    public void playSong(Cursor c) {
        String fPath = c.getString(c.getColumnIndex("_data"));
        if (fPath != null) {
            File songFile = new File(fPath);
            int position = -1;
            long[] idList = new long[mIDMappingList.size()];
            if (mIDMappingList.size() > 0) {
                for (int i = 0; i < mIDMappingList.size(); i++) {
                    HashMap<String, Object> localHashMap = mIDMappingList.get(i);
                    idList[i] = (long) Integer.parseInt((String) localHashMap.get("media_id"));
                    if (String.valueOf(c.getInt(c.getColumnIndex("_id"))).equals(localHashMap.get("downloads_id"))) {
                        position = i;
                    }
                }
            }
            if (songFile.exists() && songFile.isFile()) {
                StreamStarterActivity.startActivity(this, fPath, c.getString(c.getColumnIndex("title")), c.getString(c.getColumnIndex("artist")), c.getString(c.getColumnIndex("album")), c.getString(c.getColumnIndex("lyric")), idList, position);
                return;
            }
        }
        Toast.makeText(this, getResources().getString(R.string.ERR_FILE_NOT_EXIST), 1).show();
    }

    /* access modifiers changed from: private */
    public void shareSong(Cursor c) {
        Intent intentShare = new Intent("android.intent.action.SEND");
        int trackIDforShare = c.getInt(c.getColumnIndexOrThrow("_id"));
        intentShare.setType("audio/*");
        intentShare.putExtra("android.intent.extra.STREAM", ContentUris.withAppendedId(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, (long) trackIDforShare));
        try {
            startActivity(Intent.createChooser(intentShare, "Share via"));
        } catch (ActivityNotFoundException e) {
            Toast.makeText(this, "Unable to share", 0).show();
        }
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 1:
                return new AlertDialog.Builder(this).setTitle((int) R.string.ACTION).setItems((int) R.array.DOWNLOADED_MENU, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Cursor c;
                        Cursor c2;
                        switch (which) {
                            case 0:
                                if (MyDownloadsActivity.this.mSelectedItem != -1 && (c2 = MyDownloadsActivity.this.getContentResolver().query(Uri.parse(Downloaded.CONTENT_URI + "/" + MyDownloadsActivity.this.mSelectedItem), null, null, null, null)) != null) {
                                    try {
                                        c2.moveToFirst();
                                        if (!c2.isAfterLast()) {
                                            MyDownloadsActivity.this.playSong(c2);
                                            c2.close();
                                            return;
                                        }
                                        return;
                                    } finally {
                                        c2.close();
                                    }
                                } else {
                                    return;
                                }
                            case 1:
                                MyDownloadsActivity.this.showDialog(2);
                                return;
                            case 2:
                                MyDownloadsActivity.this.showDialog(3);
                                return;
                            case 3:
                                if (MyDownloadsActivity.this.mSelectedItem != -1 && (c = MyDownloadsActivity.this.managedQuery(Uri.parse(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI + "/" + MyDownloadsActivity.this.mSelectedItemMediaID), null, null, null, null)) != null) {
                                    try {
                                        c.moveToFirst();
                                        if (!c.isAfterLast()) {
                                            MyDownloadsActivity.this.shareSong(c);
                                            c.close();
                                            return;
                                        }
                                        return;
                                    } finally {
                                        c.close();
                                    }
                                } else {
                                    return;
                                }
                            case 4:
                                Intent intent = new Intent(MyDownloadsActivity.this, RingdroidEditActivity.class);
                                intent.setData(Uri.parse(MyDownloadsActivity.this.mFilePath));
                                MyDownloadsActivity.this.startActivity(intent);
                                return;
                            default:
                                return;
                        }
                    }
                }).create();
            case 2:
                Cursor c = getContentResolver().query(Uri.parse(Downloaded.CONTENT_URI + "/" + this.mSelectedItem), null, null, null, null);
                if (c == null) {
                    return null;
                }
                try {
                    c.moveToFirst();
                    if (c.isAfterLast()) {
                        c.close();
                        return null;
                    }
                    String title2 = c.getString(c.getColumnIndex("title"));
                    c.close();
                    if (title2 == null) {
                        return null;
                    }
                    View titleView = LayoutInflater.from(this).inflate((int) R.layout.delsong_msg, (ViewGroup) null);
                    ((TextView) titleView.findViewById(R.id.Title)).setText(String.valueOf(title2) + getString(R.string.DELETE_SONG));
                    return new AlertDialog.Builder(this).setIcon(17301543).setCustomTitle(titleView).setPositiveButton((int) R.string.ALERT_OK, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            MyDownloadsActivity.this.deleteSong(MyDownloadsActivity.this.mSelectedItem);
                        }
                    }).setNegativeButton((int) R.string.ALERT_CANCEL, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                        }
                    }).create();
                } catch (Throwable th) {
                    c.close();
                    throw th;
                }
            case 3:
                return new AlertDialog.Builder(this).setIcon(17301543).setTitle((int) R.string.ring_picker_title).setSingleChoiceItems((int) R.array.ring_types, 0, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        MyDownloadsActivity.this.ring_button_type = whichButton;
                    }
                }).setPositiveButton((int) R.string.alertdialog_ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        int ring_type;
                        Cursor c;
                        if (MyDownloadsActivity.this.ring_button_type == 0) {
                            ring_type = 1;
                        } else if (MyDownloadsActivity.this.ring_button_type == 1) {
                            ring_type = 2;
                        } else {
                            ring_type = 4;
                        }
                        String filePath = null;
                        String title = null;
                        try {
                            if (MyDownloadsActivity.this.mSelectedItem != -1) {
                                c = MyDownloadsActivity.this.getContentResolver().query(Uri.parse(Downloaded.CONTENT_URI + "/" + MyDownloadsActivity.this.mSelectedItem), null, null, null, null);
                                if (c != null) {
                                    c.moveToFirst();
                                    if (c.isAfterLast()) {
                                        c.close();
                                        return;
                                    }
                                    filePath = c.getString(c.getColumnIndex("_data"));
                                    title = c.getString(c.getColumnIndex("title"));
                                    c.close();
                                    if (filePath == null) {
                                        return;
                                    }
                                } else {
                                    return;
                                }
                            }
                            Uri uri = Ringtone.insertRingtone(MyDownloadsActivity.this.getContentResolver(), filePath, title);
                            if (uri != null) {
                                RingtoneManager.setActualDefaultRingtoneUri(MyDownloadsActivity.this, ring_type, uri);
                                Toast.makeText(MyDownloadsActivity.this, String.valueOf(title) + " set ok", 0).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        } catch (Throwable th) {
                            c.close();
                            throw th;
                        }
                    }
                }).setNegativeButton((int) R.string.alertdialog_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                }).create();
            default:
                return null;
        }
    }

    /* access modifiers changed from: private */
    public void deleteSong(int nItemID) {
        Cursor c = getContentResolver().query(Uri.parse(Downloaded.CONTENT_URI + "/" + nItemID), null, null, null, null);
        if (c != null) {
            try {
                c.moveToFirst();
                if (!c.isAfterLast()) {
                    File f = new File(c.getString(c.getColumnIndex("_data")));
                    if (f.exists() && f.isFile() && !f.delete()) {
                        f.deleteOnExit();
                    }
                    getContentResolver().delete(Uri.parse(Downloaded.CONTENT_URI + "/" + nItemID), null, null);
                    c.close();
                }
            } finally {
                c.close();
            }
        }
    }
}
