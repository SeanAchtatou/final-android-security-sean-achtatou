package com.adwhirl.adapters;

import android.app.Activity;
import android.util.Log;
import com.adwhirl.AdWhirlLayout;
import com.adwhirl.obj.Extra;
import com.adwhirl.obj.Ration;
import com.adwhirl.util.AdWhirlUtil;
import com.oneriot.OneRiotAd;
import com.oneriot.OneRiotAdActionListener;
import java.util.ArrayList;
import java.util.Iterator;

public class OneRiotAdapter extends AdWhirlAdapter implements OneRiotAdActionListener {
    protected static ArrayList<String> oneRiotContextParameters = new ArrayList<>();
    protected static int oneRiotRefreshInterval = 0;

    public OneRiotAdapter(AdWhirlLayout adWhirlLayout, Ration ration) {
        super(adWhirlLayout, ration);
    }

    public void handle() {
        Activity activity;
        AdWhirlLayout adWhirlLayout = (AdWhirlLayout) this.adWhirlLayoutReference.get();
        if (adWhirlLayout != null && (activity = adWhirlLayout.activityReference.get()) != null) {
            OneRiotAd adView = new OneRiotAd(activity, this.ration.key);
            Extra extra = adWhirlLayout.extra;
            adView.setListener(this);
            adView.setRefreshInterval(oneRiotRefreshInterval);
            adView.setReportGPS(extra.locationOn == 1);
            Iterator<String> it = oneRiotContextParameters.iterator();
            while (it.hasNext()) {
                adView.addContextParameters(it.next());
            }
            adView.loadAd(activity);
        }
    }

    public static void setOneRiotRefreshInterval(int interval) {
        oneRiotRefreshInterval = interval;
    }

    public static void setOneRiotContextParameters(ArrayList<String> contextParameters) {
        oneRiotContextParameters = contextParameters;
    }

    public void adRequestCompletedSuccessfully(OneRiotAd adView) {
        Log.d(AdWhirlUtil.ADWHIRL, "OneRiot success");
        AdWhirlLayout adWhirlLayout = (AdWhirlLayout) this.adWhirlLayoutReference.get();
        adWhirlLayout.adWhirlManager.resetRollover();
        adWhirlLayout.handler.post(new AdWhirlLayout.ViewAdRunnable(adWhirlLayout, adView));
        adWhirlLayout.rotateThreadedDelayed();
    }

    public void adRequestFailed(OneRiotAd adView) {
        Log.d(AdWhirlUtil.ADWHIRL, "OneRiot failure");
        adView.setListener((OneRiotAdActionListener) null);
        AdWhirlLayout adWhirlLayout = (AdWhirlLayout) this.adWhirlLayoutReference.get();
        if (adWhirlLayout != null) {
            adWhirlLayout.rollover();
        }
    }
}
