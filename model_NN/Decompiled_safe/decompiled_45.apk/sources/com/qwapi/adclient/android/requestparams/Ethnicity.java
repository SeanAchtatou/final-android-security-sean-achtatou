package com.qwapi.adclient.android.requestparams;

import com.qwapi.adclient.android.utils.Utils;

public enum Ethnicity {
    african_american,
    asian,
    hispanic,
    white,
    other;

    public String toString() {
        return Utils.EMPTY_STRING + ordinal();
    }
}
