package com.qwapi.adclient.android.requestparams;

import com.qwapi.adclient.android.utils.Utils;

public enum Age {
    age_12_to_17,
    age_18_24,
    age_25_34,
    age_35_49,
    age_50_54,
    ageE_55_over;

    public String toString() {
        return Utils.EMPTY_STRING + ordinal();
    }
}
