package com.qwapi.adclient.android.view;

import android.content.Context;
import android.view.View;
import com.qwapi.adclient.android.data.Ad;

public class AdViewFactory {
    public static View getAdView(Context context, Ad ad, EventDispatcher eventDispatcher, QWAdView qWAdView) {
        if (ad != null) {
            return ad.getAdType().equals("interstitial") ? new AdInterstitialView(context, ad, eventDispatcher, qWAdView) : ad.getAdType().equals(Ad.AD_TYPE_EXPANDABLE_BANNER) ? new AdExpandableView(context, ad, eventDispatcher, qWAdView, true) : new AdWebView(context, ad, eventDispatcher, qWAdView);
        }
        return null;
    }
}
