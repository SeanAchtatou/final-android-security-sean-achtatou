package com.qwapi.adclient.android;

import android.content.Context;
import android.util.Log;
import com.qwapi.adclient.android.data.Ad;
import com.qwapi.adclient.android.data.Status;
import com.qwapi.adclient.android.requestparams.AdRequestParams;
import com.qwapi.adclient.android.view.AdEventsListener;

public class AdRequestorEventsListener implements AdEventsListener {
    public void onAdClick(Context context, Ad ad) {
        Log.d(AdApiConstants.SDK, "onAdClick for Ad: " + ad.getAdType() + " : " + ad.getId());
    }

    public void onAdRequest(Context context, AdRequestParams adRequestParams) {
        Log.d(AdApiConstants.SDK, "onAdRequest for RequestParams: " + adRequestParams.toString());
    }

    public void onAdRequestFailed(Context context, AdRequestParams adRequestParams, Status status) {
        Log.d(AdApiConstants.SDK, "onAdRequestFailed for RequestParams: " + adRequestParams.toString() + " : " + status);
    }

    public void onAdRequestSuccessful(Context context, AdRequestParams adRequestParams, Ad ad) {
        Log.d(AdApiConstants.SDK, "onAdRequestSuccessful for RequestParams: " + adRequestParams.toString() + " : Ad: " + ad.getAdType() + " : " + ad.getId());
    }

    public void onDisplayAd(Context context, Ad ad) {
        Log.d(AdApiConstants.SDK, "onDisplayAd for Ad: " + ad.getAdType() + " : " + ad.getId());
    }
}
