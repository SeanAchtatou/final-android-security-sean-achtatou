package com.qwapi.adclient.android.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Status implements Serializable {
    public static final String FAILURE = "failure";
    public static final String SUCCESS = "success";
    private static final List<ErrorMessage> emptyList = new ArrayList();
    private String code;
    private List<ErrorMessage> errorMessages;

    public Status(String str, List<ErrorMessage> list) {
        this.code = str;
        this.errorMessages = list;
    }

    public static final Status getSuccess() {
        return new Status(SUCCESS, emptyList);
    }

    public String getCode() {
        return this.code;
    }

    public List<ErrorMessage> getErrorMessages() {
        return this.errorMessages;
    }

    public boolean isSuccessful() {
        return SUCCESS.equals(this.code);
    }

    public void setCode(String str) {
        this.code = str;
    }

    public void setErrorMessages(List<ErrorMessage> list) {
        this.errorMessages = list;
    }
}
