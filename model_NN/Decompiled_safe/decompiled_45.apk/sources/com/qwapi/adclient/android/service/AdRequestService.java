package com.qwapi.adclient.android.service;

import com.qwapi.adclient.android.data.AdResponse;
import com.qwapi.adclient.android.requestparams.AdRequestParams;

public interface AdRequestService {
    int count();

    AdResponse getAd(AdRequestParams adRequestParams);
}
