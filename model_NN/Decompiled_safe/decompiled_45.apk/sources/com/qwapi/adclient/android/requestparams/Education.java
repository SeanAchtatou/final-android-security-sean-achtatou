package com.qwapi.adclient.android.requestparams;

import com.qwapi.adclient.android.utils.Utils;

public enum Education {
    no_college,
    college_graduate,
    graduate_school;

    public String toString() {
        return Utils.EMPTY_STRING + ordinal();
    }
}
