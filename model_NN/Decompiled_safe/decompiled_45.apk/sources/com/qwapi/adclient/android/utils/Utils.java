package com.qwapi.adclient.android.utils;

import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import com.qwapi.adclient.android.view.AdViewConstants;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

public class Utils {
    public static final String EMPTY_STRING = "";
    public static final String UTF8 = "utf-8";
    public static final String defaultEncoding = "utf-8";

    private static void addNameValuePair(Map<String, String[]> map, String str, String str2) {
        String[] strArr;
        String[] strArr2 = map.get(str);
        if (strArr2 == null) {
            strArr = new String[]{str2};
        } else {
            String[] strArr3 = new String[(strArr2.length + 1)];
            System.arraycopy(strArr2, 0, strArr3, 0, strArr2.length);
            strArr3[strArr2.length] = str2;
            strArr = strArr3;
        }
        map.put(str, strArr);
    }

    /* JADX WARNING: Removed duplicated region for block: B:6:0x001b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean compareMaps(java.util.Map<java.lang.String, java.lang.Object> r4, java.util.Map<java.lang.String, java.lang.Object> r5) {
        /*
            r3 = 0
            int r0 = r4.size()
            int r1 = r5.size()
            if (r0 == r1) goto L_0x000d
            r0 = r3
        L_0x000c:
            return r0
        L_0x000d:
            java.util.Set r0 = r4.entrySet()
            java.util.Iterator r0 = r0.iterator()
        L_0x0015:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x0037
            java.lang.Object r4 = r0.next()
            java.util.Map$Entry r4 = (java.util.Map.Entry) r4
            java.lang.Object r1 = r4.getValue()
            if (r1 == 0) goto L_0x0035
            java.lang.Object r2 = r4.getKey()
            java.lang.Object r2 = r5.get(r2)
            boolean r1 = r1.equals(r2)
            if (r1 != 0) goto L_0x0015
        L_0x0035:
            r0 = r3
            goto L_0x000c
        L_0x0037:
            r0 = 1
            goto L_0x000c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qwapi.adclient.android.utils.Utils.compareMaps(java.util.Map, java.util.Map):boolean");
    }

    public static String decode(String str) {
        if (!isGoodString(str)) {
            return EMPTY_STRING;
        }
        try {
            return URLDecoder.decode(str, "utf-8");
        } catch (Exception e) {
            return str;
        }
    }

    public static String encode(String str) {
        return encode(str, null);
    }

    public static String encode(String str, String str2) {
        if (str == null || str.length() <= 0) {
            return EMPTY_STRING;
        }
        try {
            return URLEncoder.encode(str, isGoodString(str2) ? str2 : "utf-8");
        } catch (Exception e) {
            Log.e("Utils.encode", e.getMessage(), e);
            return str;
        }
    }

    public static String getUserAgent() {
        return EMPTY_STRING;
    }

    public static void invokeLandingPage(View view, String str) {
        if (str != null && str.length() > 0) {
            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str));
            intent.addFlags(268435456);
            try {
                view.getContext().startActivity(intent);
            } catch (Exception e) {
                Log.e("Problem", e.getMessage());
            }
        }
    }

    public static boolean isGoodString(String str) {
        return str != null && str.length() > 0 && str.trim().length() > 0;
    }

    public static Map<String, String[]> parse(String str, boolean z) {
        String decode;
        String str2;
        HashMap hashMap = new HashMap();
        if (isGoodString(str)) {
            String replaceAll = str.replaceAll("&amp;", AdViewConstants.AMP);
            int i = 0;
            int indexOf = replaceAll.indexOf(38);
            while (true) {
                int i2 = indexOf;
                int i3 = i;
                int i4 = i2;
                int indexOf2 = replaceAll.indexOf(61, i3);
                if (i4 >= 0 && indexOf2 > i4) {
                    decode = decode(replaceAll.substring(i3, i4));
                    str2 = EMPTY_STRING;
                } else if (indexOf2 != -1) {
                    String decode2 = decode(replaceAll.substring(i3, indexOf2));
                    if (z) {
                        String decode3 = decode(i4 > 0 ? replaceAll.substring(indexOf2 + 1, i4) : replaceAll.substring(indexOf2 + 1));
                        decode = decode2;
                        str2 = decode3;
                    } else {
                        String substring = i4 > 0 ? replaceAll.substring(indexOf2 + 1, i4) : replaceAll.substring(indexOf2 + 1);
                        decode = decode2;
                        str2 = substring;
                    }
                } else {
                    decode = decode(i4 > 0 ? replaceAll.substring(i3, i4) : replaceAll.substring(i3));
                    str2 = EMPTY_STRING;
                }
                if (isGoodString(decode)) {
                    addNameValuePair(hashMap, decode, str2);
                }
                if (i4 == -1) {
                    break;
                }
                i = i4 + 1;
                indexOf = replaceAll.indexOf(38, i);
            }
        }
        return hashMap;
    }

    /* JADX WARN: Failed to insert an additional move for type inference into block B:10:0x003d */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:16:0x006c */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:54:0x00de */
    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:28:0x008a */
    /* JADX INFO: additional move instructions added (6) to help type inference */
    /* JADX WARN: Type inference failed for: r2v7 */
    /* JADX WARN: Type inference failed for: r3v5 */
    /* JADX WARN: Type inference failed for: r3v9, types: [java.io.InputStream] */
    /* JADX WARN: Type inference failed for: r2v11, types: [java.io.ByteArrayOutputStream] */
    /* JADX WARN: Type inference failed for: r3v12, types: [java.io.InputStream] */
    /* JADX WARN: Type inference failed for: r2v17 */
    /* JADX WARN: Type inference failed for: r4v3, types: [java.io.ByteArrayOutputStream] */
    /* JADX WARN: Type inference failed for: r2v20 */
    /* JADX WARN: Type inference failed for: r3v16, types: [byte[]] */
    /* JADX WARN: Type inference failed for: r3v17 */
    /* JADX WARN: Type inference failed for: r2v26 */
    /* JADX WARN: Type inference failed for: r3v20 */
    /* JADX WARN: Type inference failed for: r3v22 */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x008e A[SYNTHETIC, Splitter:B:32:0x008e] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0093 A[Catch:{ Throwable -> 0x0097 }] */
    public static com.qwapi.adclient.android.utils.HttpResponse processUrl(java.lang.String r10, java.lang.String r11) {
        /*
            r9 = 0
            r8 = 0
            java.lang.String r5 = "utf-8"
            java.lang.String r3 = ""
            boolean r1 = isGoodString(r10)     // Catch:{ Throwable -> 0x0097 }
            if (r1 == 0) goto L_0x00b2
            r1 = 0
            java.net.URL r2 = new java.net.URL     // Catch:{ Throwable -> 0x0097 }
            r2.<init>(r10)     // Catch:{ Throwable -> 0x0097 }
            java.net.URLConnection r2 = r2.openConnection()     // Catch:{ Throwable -> 0x0097 }
            r4 = 1
            r2.setDoInput(r4)     // Catch:{ Throwable -> 0x0097 }
            r4 = 0
            r2.setUseCaches(r4)     // Catch:{ Throwable -> 0x0097 }
            r2.setDoOutput(r1)     // Catch:{ Throwable -> 0x0097 }
            r1 = 15000(0x3a98, float:2.102E-41)
            r2.setReadTimeout(r1)     // Catch:{ Throwable -> 0x0097 }
            boolean r1 = isGoodString(r11)     // Catch:{ Throwable -> 0x0097 }
            if (r1 == 0) goto L_0x0031
            java.lang.String r1 = "User-Agent"
            r2.setRequestProperty(r1, r11)     // Catch:{ Throwable -> 0x0097 }
        L_0x0031:
            boolean r1 = r2 instanceof java.net.HttpURLConnection     // Catch:{ Throwable -> 0x0097 }
            if (r1 == 0) goto L_0x003d
            r0 = r2
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Throwable -> 0x0097 }
            r1 = r0
            r4 = 1
            r1.setInstanceFollowRedirects(r4)     // Catch:{ Throwable -> 0x0097 }
        L_0x003d:
            if (r8 == 0) goto L_0x0063
            java.lang.String r1 = "Content-Type"
            java.lang.String r4 = "application/x-www-form-urlencoded"
            r2.setRequestProperty(r1, r4)     // Catch:{ Throwable -> 0x0097 }
            java.lang.String r1 = "Content-Length"
            int r4 = r3.length()     // Catch:{ Throwable -> 0x0097 }
            java.lang.String r4 = java.lang.String.valueOf(r4)     // Catch:{ Throwable -> 0x0097 }
            r2.setRequestProperty(r1, r4)     // Catch:{ Throwable -> 0x0097 }
            java.io.OutputStream r1 = r2.getOutputStream()     // Catch:{ Throwable -> 0x0097 }
            java.lang.String r4 = "utf-8"
            byte[] r3 = r3.getBytes(r4)     // Catch:{ Throwable -> 0x0097 }
            r1.write(r3)     // Catch:{ Throwable -> 0x0097 }
            r1.close()     // Catch:{ Throwable -> 0x0097 }
        L_0x0063:
            java.lang.String r1 = r2.getContentEncoding()     // Catch:{ Throwable -> 0x0097 }
            if (r1 != 0) goto L_0x006c
            java.lang.String r1 = "utf-8"
            r1 = r5
        L_0x006c:
            java.io.InputStream r3 = r2.getInputStream()     // Catch:{ MalformedURLException -> 0x0102, IOException -> 0x00db, Throwable -> 0x00df, all -> 0x00ec }
            java.io.ByteArrayOutputStream r4 = new java.io.ByteArrayOutputStream     // Catch:{ MalformedURLException -> 0x0106, IOException -> 0x00fc, Throwable -> 0x00f6, all -> 0x00f0 }
            r4.<init>()     // Catch:{ MalformedURLException -> 0x0106, IOException -> 0x00fc, Throwable -> 0x00f6, all -> 0x00f0 }
            r5 = 2048(0x800, float:2.87E-42)
            byte[] r5 = new byte[r5]     // Catch:{ MalformedURLException -> 0x0088, IOException -> 0x00ff, Throwable -> 0x00f9, all -> 0x00f3 }
        L_0x0079:
            r6 = 0
            r7 = 2048(0x800, float:2.87E-42)
            int r6 = r3.read(r5, r6, r7)     // Catch:{ MalformedURLException -> 0x0088, IOException -> 0x00ff, Throwable -> 0x00f9, all -> 0x00f3 }
            r7 = -1
            if (r6 == r7) goto L_0x00b4
            r7 = 0
            r4.write(r5, r7, r6)     // Catch:{ MalformedURLException -> 0x0088, IOException -> 0x00ff, Throwable -> 0x00f9, all -> 0x00f3 }
            goto L_0x0079
        L_0x0088:
            r1 = move-exception
            r2 = r4
        L_0x008a:
            throw r1     // Catch:{ all -> 0x008b }
        L_0x008b:
            r1 = move-exception
        L_0x008c:
            if (r3 == 0) goto L_0x0091
            r3.close()     // Catch:{ Throwable -> 0x0097 }
        L_0x0091:
            if (r2 == 0) goto L_0x0096
            r2.close()     // Catch:{ Throwable -> 0x0097 }
        L_0x0096:
            throw r1     // Catch:{ Throwable -> 0x0097 }
        L_0x0097:
            r1 = move-exception
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Problem in ProcessUrl,url:"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r10)
            java.lang.String r2 = r2.toString()
            java.lang.String r3 = r1.getMessage()
            android.util.Log.e(r2, r3, r1)
        L_0x00b2:
            r1 = r9
        L_0x00b3:
            return r1
        L_0x00b4:
            if (r3 == 0) goto L_0x00b9
            r3.close()     // Catch:{ Throwable -> 0x0097 }
        L_0x00b9:
            if (r4 == 0) goto L_0x00be
            r4.close()     // Catch:{ Throwable -> 0x0097 }
        L_0x00be:
            if (r4 == 0) goto L_0x010b
            byte[] r3 = r4.toByteArray()     // Catch:{ Throwable -> 0x0097 }
            java.lang.String r4 = new java.lang.String     // Catch:{ Throwable -> 0x0097 }
            r4.<init>(r3, r1)     // Catch:{ Throwable -> 0x0097 }
            r1 = r4
        L_0x00ca:
            boolean r3 = r2 instanceof java.net.HttpURLConnection     // Catch:{ Throwable -> 0x0097 }
            if (r3 == 0) goto L_0x0109
            java.net.HttpURLConnection r2 = (java.net.HttpURLConnection) r2     // Catch:{ Throwable -> 0x0097 }
            int r2 = r2.getResponseCode()     // Catch:{ Throwable -> 0x0097 }
        L_0x00d4:
            com.qwapi.adclient.android.utils.HttpResponse r3 = new com.qwapi.adclient.android.utils.HttpResponse     // Catch:{ Throwable -> 0x0097 }
            r3.<init>(r1, r2)     // Catch:{ Throwable -> 0x0097 }
            r1 = r3
            goto L_0x00b3
        L_0x00db:
            r1 = move-exception
            r2 = r9
            r3 = r9
        L_0x00de:
            throw r1     // Catch:{ all -> 0x008b }
        L_0x00df:
            r1 = move-exception
            r2 = r9
            r3 = r9
        L_0x00e2:
            java.io.IOException r4 = new java.io.IOException     // Catch:{ all -> 0x008b }
            java.lang.String r1 = r1.getMessage()     // Catch:{ all -> 0x008b }
            r4.<init>(r1)     // Catch:{ all -> 0x008b }
            throw r4     // Catch:{ all -> 0x008b }
        L_0x00ec:
            r1 = move-exception
            r2 = r9
            r3 = r9
            goto L_0x008c
        L_0x00f0:
            r1 = move-exception
            r2 = r9
            goto L_0x008c
        L_0x00f3:
            r1 = move-exception
            r2 = r4
            goto L_0x008c
        L_0x00f6:
            r1 = move-exception
            r2 = r9
            goto L_0x00e2
        L_0x00f9:
            r1 = move-exception
            r2 = r4
            goto L_0x00e2
        L_0x00fc:
            r1 = move-exception
            r2 = r9
            goto L_0x00de
        L_0x00ff:
            r1 = move-exception
            r2 = r4
            goto L_0x00de
        L_0x0102:
            r1 = move-exception
            r2 = r9
            r3 = r9
            goto L_0x008a
        L_0x0106:
            r1 = move-exception
            r2 = r9
            goto L_0x008a
        L_0x0109:
            r2 = r8
            goto L_0x00d4
        L_0x010b:
            r1 = r9
            goto L_0x00ca
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qwapi.adclient.android.utils.Utils.processUrl(java.lang.String, java.lang.String):com.qwapi.adclient.android.utils.HttpResponse");
    }
}
