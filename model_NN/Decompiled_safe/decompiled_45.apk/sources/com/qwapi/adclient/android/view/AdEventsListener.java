package com.qwapi.adclient.android.view;

import android.content.Context;
import com.qwapi.adclient.android.data.Ad;
import com.qwapi.adclient.android.data.Status;
import com.qwapi.adclient.android.requestparams.AdRequestParams;

public interface AdEventsListener {
    void onAdClick(Context context, Ad ad);

    void onAdRequest(Context context, AdRequestParams adRequestParams);

    void onAdRequestFailed(Context context, AdRequestParams adRequestParams, Status status);

    void onAdRequestSuccessful(Context context, AdRequestParams adRequestParams, Ad ad);

    void onDisplayAd(Context context, Ad ad);
}
