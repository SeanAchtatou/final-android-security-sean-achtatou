package com.qwapi.adclient.android.data;

import android.util.Log;
import com.qwapi.adclient.android.utils.Utils;
import com.qwapi.adclient.android.view.AdViewConstants;
import java.io.Serializable;
import java.util.Map;

public class Image implements Serializable {
    private String altText;
    private int height;
    private String url;
    private int width;

    public Image(String str, int i, int i2, String str2) {
        this.url = str;
        this.width = i;
        this.height = i2;
        this.altText = str2;
    }

    public static String encodeUrl(String str) {
        String replace = str.replace("&amp;", AdViewConstants.AMP);
        if (!(replace.indexOf(63) == -1 || replace.indexOf("location") == -1)) {
            Map<String, String[]> parse = Utils.parse(replace.substring(replace.indexOf(63) + 1), false);
            StringBuffer stringBuffer = new StringBuffer(replace.substring(0, replace.indexOf(63) + 1));
            for (Map.Entry next : parse.entrySet()) {
                if (((String) next.getKey()).equals("location")) {
                    stringBuffer.append((String) next.getKey()).append('=').append(Utils.encode(((String[]) next.getValue())[0]));
                } else {
                    stringBuffer.append((String) next.getKey()).append('=').append(((String[]) next.getValue())[0]);
                }
                stringBuffer.append('&');
            }
            replace = stringBuffer.toString();
        }
        Log.d("QuattroWirelessSDK/2.1 image url:", replace);
        return replace;
    }

    public String getAltText() {
        return this.altText;
    }

    public int getHeight() {
        return this.height;
    }

    public String getUrl() {
        return this.url;
    }

    public int getWidth() {
        return this.width;
    }
}
