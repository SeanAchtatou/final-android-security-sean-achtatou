package com.qwapi.adclient.android.service;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.qwapi.adclient.android.view.QWAdView;
import java.lang.ref.WeakReference;

public class AdRequestThread extends Thread {
    private static final int AD_REQUEST_MSG = 7777;
    WeakReference<QWAdView> adView = null;
    boolean first = true;
    private Looper looper = null;
    private Handler mHandler;
    private volatile boolean stopRequested = false;

    public AdRequestThread(QWAdView qWAdView) {
        this.adView = new WeakReference<>(qWAdView);
    }

    public Handler getHandler() {
        return this.mHandler;
    }

    public void run() {
        while (!this.stopRequested) {
            Looper.prepare();
            this.mHandler = new Handler() {
                boolean first = true;

                public void handleMessage(Message message) {
                    if (AdRequestThread.this.adView.get().isStartDisplayingAds()) {
                        AdRequestThread.this.adView.get().prepareAd();
                        AdRequestThread.this.adView.get().showNextAd();
                    }
                }
            };
            if (this.first) {
                this.mHandler.sendEmptyMessage(AD_REQUEST_MSG);
                this.first = false;
            }
            this.looper = Looper.myLooper();
            Looper.loop();
        }
    }

    public void stopThread() {
        this.stopRequested = true;
        if (this.looper != null) {
            this.looper.quit();
        }
    }
}
