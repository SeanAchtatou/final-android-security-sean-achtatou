package com.qwapi.adclient.android;

import com.qwapi.adclient.android.utils.Utils;
import java.util.HashMap;
import java.util.Map;

public class AdApiConfig {
    private static final Map<ExecutionMode, String> API_URLS = new HashMap();
    private static final Map<ExecutionMode, String> PROXY_URLS = new HashMap();
    private static String defaultSiteId = Utils.EMPTY_STRING;
    private static ExecutionMode mode = ExecutionMode.LIVE;
    private static String publisherId = Utils.EMPTY_STRING;
    public static boolean testMode = false;

    public enum ExecutionMode {
        LIVE,
        STAGING,
        QA
    }

    static {
        API_URLS.put(ExecutionMode.LIVE, "http://ad.qwapi.com/adserver/");
        API_URLS.put(ExecutionMode.STAGING, "http://stg-ad.qwapi.com/adserver/");
        API_URLS.put(ExecutionMode.QA, "http://ad-qa.qwapi.com/adserver/");
        PROXY_URLS.put(ExecutionMode.LIVE, "http://site.qwapi.com/siteserver/sdkproxy.jsp");
        PROXY_URLS.put(ExecutionMode.STAGING, "http://stg-site.qwapi.com/siteserver/sdkproxy.jsp");
        PROXY_URLS.put(ExecutionMode.QA, "http://qa-site.qwapi.com/siteserver/sdkproxy.jsp");
    }

    public static String getAPIUrl() {
        return API_URLS.get(mode);
    }

    public static String getDefaultSiteId() {
        return defaultSiteId;
    }

    public static ExecutionMode getExecutionMode() {
        return mode;
    }

    public static String getProxyUrl() {
        return PROXY_URLS.get(mode);
    }

    public static String getPublihserId() {
        return publisherId;
    }

    public static boolean getTestMode() {
        return testMode;
    }

    public static void setDefaultSiteId(String str) {
        defaultSiteId = str;
    }

    public static void setExecutionMode(ExecutionMode executionMode) {
        mode = executionMode;
    }

    public static void setPublisherid(String str) {
        publisherId = str;
    }

    public static void setTestMode(boolean z) {
        testMode = z;
    }
}
