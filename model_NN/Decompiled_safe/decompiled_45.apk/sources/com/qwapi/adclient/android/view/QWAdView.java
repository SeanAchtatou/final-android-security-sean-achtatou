package com.qwapi.adclient.android.view;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ViewAnimator;
import com.qwapi.adclient.android.AdApiConfig;
import com.qwapi.adclient.android.AdApiConstants;
import com.qwapi.adclient.android.AdApiRegistration;
import com.qwapi.adclient.android.DeviceContext;
import com.qwapi.adclient.android.R;
import com.qwapi.adclient.android.data.Ad;
import com.qwapi.adclient.android.data.AdResponse;
import com.qwapi.adclient.android.requestparams.AdRequestParams;
import com.qwapi.adclient.android.requestparams.Age;
import com.qwapi.adclient.android.requestparams.AnimationType;
import com.qwapi.adclient.android.requestparams.DisplayMode;
import com.qwapi.adclient.android.requestparams.Education;
import com.qwapi.adclient.android.requestparams.Ethnicity;
import com.qwapi.adclient.android.requestparams.Gender;
import com.qwapi.adclient.android.requestparams.Income;
import com.qwapi.adclient.android.requestparams.MediaType;
import com.qwapi.adclient.android.requestparams.Placement;
import com.qwapi.adclient.android.requestparams.RequestMode;
import com.qwapi.adclient.android.service.AdRefresherThread;
import com.qwapi.adclient.android.service.AdRequestService;
import com.qwapi.adclient.android.service.AdRequestThread;
import com.qwapi.adclient.android.service.AgeAdRefresherThread;
import com.qwapi.adclient.android.service.BatchAdRequestService;
import com.qwapi.adclient.android.service.SingleAdRequestService;
import java.lang.ref.WeakReference;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class QWAdView extends ViewAnimator {
    private static final int AD_REQUEST_MSG = 7777;
    private static final int AGE_AD_MSG = 8888;
    private static final int MIN_INTERVAL = 30;
    private static final int MSG = 9999;
    /* access modifiers changed from: private */
    public Ad _ad;
    private Activity activity;
    private int adInterval;
    private AdRefresherThread adRefresher;
    private AdRequestParams adRequestParams;
    private AdRequestThread adRequestThread;
    private AdResponse adResponse;
    AdRequestService adService = null;
    private AgeAdRefresherThread ageAdRefresher;
    private String defaultAdImage;
    private AdImageView defaultAdView;
    private DeviceContext deviceContext;
    private DisplayMode displayMode;
    /* access modifiers changed from: private */
    public EventDispatcher eventDispatcher;
    private String eventListenerClassName;
    private SkipListener interstitialSkipListener = new InterstitialViewSkipListener(this);
    private boolean mFirstTime;
    private int mWhichChild;
    private RequestMode requestMode;
    private boolean startDisplayingAds = false;
    private boolean suspendAutoRefresh;
    boolean useBatch = false;

    static class InterstitialViewSkipListener implements SkipListener {
        private WeakReference<QWAdView> parentView;

        public InterstitialViewSkipListener(QWAdView qWAdView) {
            this.parentView = new WeakReference<>(qWAdView);
        }

        public void onSkip() {
            if (this.parentView.get() != null) {
                try {
                    this.parentView.get().clearViews();
                } catch (Exception e) {
                    Log.e(AdApiConstants.SDK, e.getMessage());
                }
            }
        }
    }

    public QWAdView(Context context, AttributeSet attributeSet) throws QWAdViewException {
        super(context, attributeSet);
        String str;
        AdEventsListener adEventsListener;
        boolean z;
        boolean z2;
        this.deviceContext = new DeviceContext(context);
        this.adRequestParams = new AdRequestParams(this.deviceContext);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.QWAdView);
        String string = obtainStyledAttributes.getString(13);
        if (string != null) {
            try {
                setRequestMode(RequestMode.valueOf(string.toString()));
            } catch (IllegalArgumentException e) {
                Log.e(AdApiConstants.SDK, "invalid value for requestMode : [" + ((Object) string) + "]");
                throw new QWAdViewException("invalid value for requestMode : [" + ((Object) string) + "]");
            }
        } else {
            Log.d(AdApiConstants.SDK, "requestMode not provided using single request mode");
            setRequestMode(RequestMode.single);
        }
        String string2 = obtainStyledAttributes.getString(1);
        if (string2 != null) {
            if (getRequestMode() == RequestMode.batch) {
                String[] split = string2.toString().split(",");
                int i = 0;
                while (i < split.length) {
                    String[] split2 = split[i].split(":");
                    if (split2.length != 2) {
                        Log.e(AdApiConstants.SDK, "invalid value for mediaType in batch request mode : [" + ((Object) string2) + "]");
                        throw new QWAdViewException("invalid value for mediaType in batch request mode : [" + ((Object) string2) + "]");
                    }
                    try {
                        addMediaType(MediaType.valueOf(split2[0]), Integer.parseInt(split2[1]));
                        i++;
                    } catch (IllegalArgumentException e2) {
                        Log.e(AdApiConstants.SDK, "invalid value for mediaType in batch request mode : [" + split2[0] + "] count : [" + split2[1] + "]");
                        throw new QWAdViewException("invalid value for mediaType in batch request mode : [" + split2[0] + "] count : [" + split2[1] + "]");
                    }
                }
            } else {
                String[] split3 = string2.toString().split(",");
                int i2 = 0;
                while (i2 < split3.length) {
                    try {
                        addMediaType(MediaType.valueOf(split3[i2]), 1);
                        i2++;
                    } catch (IllegalArgumentException e3) {
                        Log.e(AdApiConstants.SDK, "invalid value for mediaType in single request mode : [" + split3[i2] + "]");
                        throw new QWAdViewException("invalid value for mediaType in single request mode : [" + split3[i2] + "]");
                    }
                }
            }
            String string3 = obtainStyledAttributes.getString(0);
            if (string3 != null) {
                try {
                    setPlacement(Placement.valueOf(string3.toString()));
                } catch (IllegalArgumentException e4) {
                    Log.e(AdApiConstants.SDK, "invalid value for placement : [" + ((Object) string3) + "]");
                    throw new QWAdViewException("invalid value for placement : [" + ((Object) string3) + "]");
                }
            }
            String string4 = obtainStyledAttributes.getString(3);
            if (string4 != null) {
                try {
                    int parseInt = Integer.parseInt(string4.toString());
                    if (parseInt < 30) {
                        Log.w(AdApiConstants.SDK, "adInterval is smaller than the min interval of [30] setting interval to [30]");
                        parseInt = 30;
                    }
                    setAdInterval(parseInt);
                } catch (Exception e5) {
                    Log.e(AdApiConstants.SDK, "Problem parsing refreshInterval : [" + ((Object) string4) + "]");
                    throw new QWAdViewException("Problem parsing refreshInterval : [" + ((Object) string4) + "]");
                }
            } else {
                setAdInterval(30);
            }
            String string5 = obtainStyledAttributes.getString(5);
            if (string5 == null || string5.length() <= 0) {
                Log.e(AdApiConstants.SDK, "siteId is a required parameter");
                throw new QWAdViewException("siteId is a required parameter");
            }
            setSiteId(string5.toString());
            String string6 = obtainStyledAttributes.getString(6);
            if (string6 == null || string6.length() <= 0) {
                Log.e(AdApiConstants.SDK, "publisherId is a required parameter");
                throw new QWAdViewException("publisherId is a required parameter");
            }
            setPublisherId(string6.toString());
            String string7 = obtainStyledAttributes.getString(4);
            if (string7 != null) {
                try {
                    setAnimationType(context, AnimationType.valueOf(string7.toString()));
                } catch (IllegalArgumentException e6) {
                    Log.e(AdApiConstants.SDK, "invalid value for animation : [" + ((Object) string7) + "]");
                    throw new QWAdViewException("invalid value for animation : [" + ((Object) string7) + "]");
                }
            } else {
                setAnimationType(context, AnimationType.slide);
                Log.d(AdApiConstants.SDK, "animation parameter was not provided using default value of slide");
            }
            String string8 = obtainStyledAttributes.getString(9);
            if (string8 != null) {
                setSection(string8.toString());
            }
            String string9 = obtainStyledAttributes.getString(2);
            if (string9 != null) {
                try {
                    setDisplayMode(DisplayMode.valueOf(string9.toString()));
                } catch (IllegalArgumentException e7) {
                    Log.e(AdApiConstants.SDK, "invalid value for displayMode : [" + ((Object) string9) + "]");
                    throw new QWAdViewException("invalid value for displayMode : [" + ((Object) string9) + "]");
                }
            } else {
                setDisplayMode(DisplayMode.normal);
                Log.d(AdApiConstants.SDK, "displayMode parameter was not provided using default value of autoRotate");
            }
            Drawable drawable = obtainStyledAttributes.getDrawable(7);
            if (drawable != null) {
                String string10 = obtainStyledAttributes.getString(8);
                setDefaultAd(drawable, string10 == null ? null : string10.toString());
            }
            String string11 = obtainStyledAttributes.getString(10);
            if (string11 != null) {
                setEventListenerClassName(string11.toString());
            }
            if (getEventListenerClassName() == null || getEventListenerClassName().length() <= 0) {
                str = null;
                adEventsListener = null;
            } else {
                try {
                    adEventsListener = (AdEventsListener) Class.forName(getEventListenerClassName()).newInstance();
                    str = null;
                } catch (InstantiationException e8) {
                    str = "adEventsListenerClass : Instantiation issue while trying to instantiate : [" + getEventListenerClassName() + "]";
                    adEventsListener = null;
                } catch (IllegalAccessException e9) {
                    str = "adEventsListenerClass : Illegal Access issue while trying to instantiate : [" + getEventListenerClassName() + "]";
                    adEventsListener = null;
                } catch (ClassNotFoundException e10) {
                    str = "adEventsListenerClass : Class Not Found, issue while trying to instantiate : [" + getEventListenerClassName() + "]";
                    adEventsListener = null;
                }
            }
            if (str != null) {
                Log.e(AdApiConstants.SDK, str);
                throw new QWAdViewException(str);
            }
            String string12 = obtainStyledAttributes.getString(12);
            if (string12 != null) {
                try {
                    z = Boolean.parseBoolean(string12.toString());
                } catch (Exception e11) {
                    Log.e(AdApiConstants.SDK, "Invalid renderOnCreate:" + e11.getMessage());
                    throw new QWAdViewException("Invalid renderOnCreate : " + e11.getMessage());
                }
            } else {
                Log.d(AdApiConstants.SDK, "renderOnCreate parameter not provided using default value of true");
                z = true;
            }
            String string13 = obtainStyledAttributes.getString(11);
            if (string13 != null) {
                try {
                    z2 = Boolean.parseBoolean(string13.toString());
                } catch (Exception e12) {
                    Log.e(AdApiConstants.SDK, "Invalid testMode value must be true|false : [" + ((Object) string13) + "]");
                    throw new QWAdViewException("Invalid testMode value must be true|false : [" + ((Object) string13) + "]");
                }
            } else {
                Log.d(AdApiConstants.SDK, "testMode parameter not provided using default value of false");
                z2 = false;
            }
            String string14 = obtainStyledAttributes.getString(14);
            if (string14 != null) {
                try {
                    setBackgroundColor(Color.parseColor(string14.toString()));
                } catch (IllegalArgumentException e13) {
                    Log.e(AdApiConstants.SDK, "Invalid bgColor value must be a valid color format as defined by android.graphics.Color.parseColor(String)  : [" + ((Object) string14) + "]");
                    throw new QWAdViewException("Invalid bgColor value must be a valid color format as defined by android.graphics.Color.parseColor(String) : [" + ((Object) string14) + "]");
                }
            } else {
                setBackgroundColor(-16777216);
            }
            String string15 = obtainStyledAttributes.getString(15);
            if (string15 != null) {
                try {
                    setTextColor(Color.parseColor(string15.toString()));
                } catch (IllegalArgumentException e14) {
                    Log.e(AdApiConstants.SDK, "Invalid textColor value must be a valid color format as defined by android.graphics.Color.parseColor(String)  : [" + ((Object) string14) + "]");
                    throw new QWAdViewException("Invalid textColor value must be a valid color format as defined by android.graphics.Color.parseColor(String) : [" + ((Object) string14) + "]");
                }
            } else {
                setTextColor(-256);
            }
            setTestMode(z2);
            init(context, adEventsListener, z);
            return;
        }
        Log.e(AdApiConstants.SDK, "mediaType is a required property.");
        throw new QWAdViewException("mediaType is a required property.");
    }

    public QWAdView(Context context, String str, String str2, MediaType mediaType, Placement placement, DisplayMode displayMode2, int i, AnimationType animationType, AdEventsListener adEventsListener, boolean z) {
        super(context);
        this.deviceContext = new DeviceContext(context);
        this.adRequestParams = new AdRequestParams(this.deviceContext);
        this.displayMode = displayMode2;
        setAnimationType(context, animationType);
        setLayoutParams(new FrameLayout.LayoutParams(-1, -2));
        setSiteId(str);
        setPublisherId(str2);
        addMediaType(mediaType, 1);
        setPlacement(placement);
        setAdInterval(i);
        setAnimationType(context, animationType);
        init(context, adEventsListener, z);
        setPadding(0, 0, 0, 0);
        setBackgroundColor(-16777216);
    }

    /* access modifiers changed from: private */
    public void clearViews() {
        removeAllViews();
        addView(new EmptyView(getContext()));
        invalidate();
    }

    private int getNextChildIndex() {
        if (!this.mFirstTime) {
            return this.mWhichChild == 0 ? 1 : 0;
        }
        this.mWhichChild = 0;
        return this.mWhichChild;
    }

    private void init(Context context, AdEventsListener adEventsListener, boolean z) {
        if (context instanceof Activity) {
            this.activity = (Activity) context;
        }
        this.mFirstTime = true;
        addView(new EmptyView(context));
        addView(new EmptyView(context));
        this.eventDispatcher = new EventDispatcher(this.activity, adEventsListener, false);
        reset();
        this.startDisplayingAds = z;
        prepareAdRequester();
        if (!prepareAdFetcher()) {
            prepareAgeAdRefresher();
        }
        setPadding(0, 0, 0, 0);
    }

    public void addMediaType(MediaType mediaType, int i) {
        this.adRequestParams.addMediaType(mediaType, i);
    }

    public void dispatchWindowFocusChanged(boolean z) {
        super.dispatchWindowFocusChanged(z);
        this.suspendAutoRefresh = !z;
    }

    public void displayNextAd() {
        this.startDisplayingAds = true;
        prepareAdRequester();
        if (this.adRequestThread.getHandler() != null) {
            this.adRequestThread.getHandler().sendEmptyMessage(AD_REQUEST_MSG);
        }
    }

    public AdEventsListener getAdEventsListener() {
        return this.eventDispatcher.getEventListener();
    }

    public int getAdInterval() {
        return this.adInterval;
    }

    public Age getAge() {
        return this.adRequestParams.getAge();
    }

    public String getAreaCode() {
        return this.adRequestParams.getAreaCode();
    }

    public Color getBackgroundColor() {
        return this.adRequestParams.getBackgroundColor();
    }

    public Date getBirthDate() {
        return this.adRequestParams.getBirthDate();
    }

    public List<AdResponse> getCachedBatchAds() {
        if (this.adService instanceof BatchAdRequestService) {
            return ((BatchAdRequestService) this.adService).getAllAds();
        }
        return null;
    }

    public String getDefaultAdImage() {
        return this.defaultAdImage;
    }

    public DisplayMode getDisplayMode() {
        return this.displayMode;
    }

    public String getDmaCode() {
        return this.adRequestParams.getDmaCode();
    }

    public Education getEducation() {
        return this.adRequestParams.getEducation();
    }

    public Ethnicity getEthnicity() {
        return this.adRequestParams.getEthnicity();
    }

    public String getEventListenerClassName() {
        return this.eventListenerClassName;
    }

    public Gender getGender() {
        return this.adRequestParams.getGender();
    }

    public Income getIncome() {
        return this.adRequestParams.getIncome();
    }

    public Map<MediaType, Integer> getMediaTypes() {
        return this.adRequestParams.getMediaTypes();
    }

    public View getNextView() {
        return getChildAt(getNextChildIndex());
    }

    public Placement getPlacement() {
        return this.adRequestParams.getPlacement();
    }

    public String getPublisherId() {
        return this.adRequestParams.getPubId();
    }

    public RequestMode getRequestMode() {
        return this.requestMode;
    }

    public String getSection() {
        return this.adRequestParams.getSection();
    }

    public String getSiteId() {
        return this.adRequestParams.getSiteId();
    }

    public boolean getTestMode() {
        return this.adRequestParams.getTestMode();
    }

    public String getTextColor() {
        return this.adRequestParams.getTextColor();
    }

    public String getZipCode() {
        return this.adRequestParams.getZipCode();
    }

    public boolean isStartDisplayingAds() {
        return this.startDisplayingAds;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        this.suspendAutoRefresh = false;
        if (this.adRefresher == null) {
            prepareAdFetcher();
        }
        prepareAdRequester();
        super.onAttachedToWindow();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        if (this.ageAdRefresher != null) {
            this.ageAdRefresher.stopThread();
            this.ageAdRefresher = null;
        }
        if (this.adRefresher != null) {
            this.adRefresher.stopThread();
            this.adRefresher = null;
        }
        if (this.adRequestThread != null) {
            this.adRequestThread.stopThread();
            this.adRequestThread = null;
        }
        clearViews();
        super.onDetachedFromWindow();
    }

    public void prepareAd() {
        if (!AdApiRegistration.isRegistered(getContext())) {
            AdApiRegistration.registerApplication(getContext());
        }
        if (this.useBatch) {
            if (this.adService == null) {
                synchronized (this) {
                    BatchAdRequestService batchAdRequestService = new BatchAdRequestService(this.activity);
                    batchAdRequestService.setBatchSize(5);
                    this.adService = batchAdRequestService;
                }
            }
        } else if (this.adService == null) {
            synchronized (this) {
                this.adService = new SingleAdRequestService();
            }
        }
        this.eventDispatcher.onAdRequest(this.adRequestParams);
        this.adResponse = this.adService.getAd(this.adRequestParams);
        if (this.adResponse != null) {
            this._ad = this.adResponse.getAd(0);
            if (this._ad != null) {
                this.eventDispatcher.onAdRequestSuccessful(this.adRequestParams, this._ad);
            } else {
                this.eventDispatcher.onAdRequestFailed(this.adRequestParams, this.adResponse.getStatus());
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean prepareAdFetcher() {
        if (this.displayMode != DisplayMode.autoRotate) {
            return false;
        }
        this.adRefresher = new AdRefresherThread(this, this.displayMode, this.adInterval);
        this.adRefresher.start();
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean prepareAdRequester() {
        if (this.adRequestThread != null) {
            return true;
        }
        this.adRequestThread = new AdRequestThread(this);
        this.adRequestThread.start();
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean prepareAgeAdRefresher() {
        if (this.displayMode != DisplayMode.aged) {
            return false;
        }
        this.ageAdRefresher = new AgeAdRefresherThread(this, this.displayMode, this.adInterval);
        this.ageAdRefresher.start();
        return true;
    }

    public void removeMediaType(MediaType mediaType) {
        this.adRequestParams.removeMediaType(mediaType);
    }

    public void reset() {
        this.mFirstTime = true;
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            if (childAt != null) {
                childAt.setVisibility(8);
            }
        }
    }

    public void resetAutoRefresh() {
        if (this.ageAdRefresher != null) {
            this.ageAdRefresher.stopThread();
            this.ageAdRefresher = null;
        }
        if (this.adRefresher != null) {
            this.adRefresher.stopThread();
            this.adRefresher = null;
        }
        if (!prepareAdFetcher()) {
            prepareAgeAdRefresher();
        }
    }

    public void resumeRefresherThreads() {
        if (this.ageAdRefresher != null) {
            this.ageAdRefresher.resumeThread();
        }
        if (this.adRefresher != null) {
            this.adRefresher.resumeThread();
        }
    }

    public void setAdEventsListener(AdEventsListener adEventsListener, boolean z) {
        this.eventDispatcher.setEventListener(adEventsListener);
        this.eventDispatcher.setRunOnUIThread(z);
    }

    public void setAdInterval(int i) {
        int i2 = 30;
        if (i >= 30) {
            i2 = i;
        }
        this.adInterval = i2;
    }

    public void setAdResponse(AdResponse adResponse2) {
        this.adResponse = adResponse2;
    }

    public void setAdView(View view) {
        AdImageView adImageView;
        if (view == null) {
            adImageView = this.defaultAdView != null ? this.defaultAdView : new EmptyView(getContext());
        } else if (view instanceof AdInterstitialView) {
            ((AdInterstitialView) view).setSkipListener(this.interstitialSkipListener);
            this.eventDispatcher.onDisplayAd(this._ad);
            adImageView = view;
        } else if (view instanceof AdWebView) {
            this.eventDispatcher.onDisplayAd(this._ad);
            adImageView = view;
        } else {
            if (view instanceof AdExpandableView) {
                this.eventDispatcher.onDisplayAd(this._ad);
            }
            adImageView = view;
        }
        boolean z = true;
        View childAt = getChildAt(this.mWhichChild);
        if (childAt != null) {
            if ((adImageView instanceof AdWebView) && (childAt instanceof AdWebView)) {
                z = false;
            } else if ((adImageView instanceof AdInterstitialView) && (childAt instanceof AdInterstitialView)) {
                z = false;
            } else if ((adImageView instanceof EmptyView) && (childAt instanceof EmptyView)) {
                z = false;
            }
            if (z) {
                clearViews();
            }
        }
        if (adImageView.getLayoutParams() == null) {
            addView(adImageView, getNextChildIndex(), new FrameLayout.LayoutParams(-1, -2));
        } else {
            addView(adImageView, getNextChildIndex(), adImageView.getLayoutParams());
        }
        setDisplayedChild(getNextChildIndex());
        this.mWhichChild = getNextChildIndex();
        if (adImageView instanceof AdInterstitialView) {
            adImageView.getParent().bringChildToFront(adImageView);
        }
        if (childAt != null && (childAt instanceof AdInterstitialView)) {
            ((AdInterstitialView) childAt).cleanup();
        }
    }

    public void setAge(Age age) {
        this.adRequestParams.setAge(age);
    }

    public void setAnimationType(Context context, AnimationType animationType) {
        try {
            if (animationType == AnimationType.fade) {
                setInAnimation(context, 17432576);
                setOutAnimation(context, 17432577);
            } else if (animationType == AnimationType.slide) {
                setInAnimation(context, 17432578);
                setOutAnimation(context, 17432579);
            } else if (animationType == AnimationType.hyperspace) {
                setInAnimation(context, tai.haod.rmbd.R.anim.footer_appear);
                setOutAnimation(context, tai.haod.rmbd.R.anim.footer_disappear);
            } else if (animationType == AnimationType.pushUp) {
                setInAnimation(context, R.anim.push_up_in);
                setOutAnimation(context, R.anim.push_up_out);
            } else if (animationType == AnimationType.pushLeft) {
                setInAnimation(context, R.anim.push_left_in);
                setOutAnimation(context, R.anim.push_left_out);
            }
        } catch (Exception e) {
            Log.w(AdApiConstants.SDK, "Unable to set the requested Animation Type:" + animationType);
            setInAnimation(context, 17432578);
            setOutAnimation(context, 17432579);
        }
    }

    public void setAreaCode(String str) {
        this.adRequestParams.setAreaCode(str);
    }

    public void setBackgroundColor(Color color) {
        this.adRequestParams.setBackgroundColor(color);
    }

    public void setBirthDate(Date date) {
        this.adRequestParams.setBirthDate(date);
    }

    public void setDefaultAd(Drawable drawable, String str) {
        if (this.defaultAdView == null && drawable != null) {
            this.defaultAdView = new AdImageView(getContext(), drawable, str);
        }
    }

    public void setDefaultAdImage(String str) {
        this.defaultAdImage = str;
    }

    public void setDisplayMode(DisplayMode displayMode2) {
        this.displayMode = displayMode2;
    }

    public void setDmaCode(String str) {
        this.adRequestParams.setDmaCode(str);
    }

    public void setEducation(Education education) {
        this.adRequestParams.setEducation(education);
    }

    public void setEthnicity(Ethnicity ethnicity) {
        this.adRequestParams.setEthnicity(ethnicity);
    }

    public void setEventListenerClassName(String str) {
        this.eventListenerClassName = str;
    }

    public void setGender(Gender gender) {
        this.adRequestParams.setGender(gender);
    }

    public void setIncome(Income income) {
        this.adRequestParams.setIncome(income);
    }

    public void setPlacement(Placement placement) {
        this.adRequestParams.setPlacement(placement);
    }

    public void setPublisherId(String str) {
        AdApiConfig.setPublisherid(str);
        this.adRequestParams.setPubId(str);
    }

    public void setRequestMode(RequestMode requestMode2) {
        if (requestMode2 == null) {
            Log.e(AdApiConstants.SDK, "requestMode not provided using single requestMode");
            this.requestMode = RequestMode.single;
            this.useBatch = false;
        } else {
            if (requestMode2 == RequestMode.batch) {
                this.useBatch = true;
            } else {
                this.useBatch = false;
            }
            this.requestMode = requestMode2;
        }
        if (this.adService instanceof BatchAdRequestService) {
            ((BatchAdRequestService) this.adService).closeDatabase();
        }
        this.adService = null;
    }

    public void setSection(String str) {
        this.adRequestParams.setSection(str);
    }

    public void setSiteId(String str) {
        AdApiConfig.setDefaultSiteId(str);
        this.adRequestParams.setSiteId(str);
    }

    public void setTestMode(boolean z) {
        this.adRequestParams.setTestMode(z);
    }

    public void setTextColor(int i) {
        this.adRequestParams.setTextColor(i);
    }

    public void setZipCode(String str) {
        this.adRequestParams.setZipCode(str);
    }

    public void showNextAd() {
        this.activity.runOnUiThread(new Runnable() {
            public void run() {
                View view;
                if (QWAdView.this._ad != null) {
                    view = AdViewFactory.getAdView(QWAdView.this.getContext(), QWAdView.this._ad, QWAdView.this.eventDispatcher, QWAdView.this);
                } else {
                    Log.d(AdApiConstants.SDK, "No ad in showNextAd");
                    view = null;
                }
                QWAdView.this.setAdView(view);
                QWAdView.this.invalidate();
                Ad unused = QWAdView.this._ad = null;
            }
        });
    }

    public void suspendRefresherThreads() {
        if (this.ageAdRefresher != null) {
            this.ageAdRefresher.suspendThread();
        }
        if (this.adRefresher != null) {
            this.adRefresher.suspendThread();
        }
    }

    public void testParent() {
        Log.d("QWAdview", "testParent successful");
    }
}
