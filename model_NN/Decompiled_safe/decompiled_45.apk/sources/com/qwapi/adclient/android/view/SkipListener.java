package com.qwapi.adclient.android.view;

public interface SkipListener {
    void onSkip();
}
