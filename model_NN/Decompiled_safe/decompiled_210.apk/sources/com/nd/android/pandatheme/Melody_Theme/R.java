package com.nd.android.pandatheme.Melody_Theme;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int bg = 2130837504;
        public static final int btn_apply = 2130837505;
        public static final int btn_focused = 2130837506;
        public static final int btn_normal = 2130837507;
        public static final int btn_pressed = 2130837508;
        public static final int clock_dial = 2130837509;
        public static final int clock_hour = 2130837510;
        public static final int clock_minute = 2130837511;
        public static final int com_android_alarmclock_com_android_alarmclock_alarmclock = 2130837512;
        public static final int com_android_browser_com_android_browser_browseractivity = 2130837513;
        public static final int com_android_calculator2_com_android_calculator2_calculator = 2130837514;
        public static final int com_android_camera_com_android_camera_camera = 2130837515;
        public static final int com_android_camera_com_android_camera_gallerypicker = 2130837516;
        public static final int com_android_camera_com_android_camera_videocamera = 2130837517;
        public static final int com_android_contacts_com_android_contacts_dialtactsactivity = 2130837518;
        public static final int com_android_contacts_com_android_contacts_dialtactscontactsentryactivity = 2130837519;
        public static final int com_android_deskclock_com_android_deskclock_deskclock = 2130837520;
        public static final int com_android_email_com_android_email_activity_welcome = 2130837521;
        public static final int com_android_mms_com_android_mms_ui_conversationlist = 2130837522;
        public static final int com_android_music_com_android_music_musicbrowseractivity = 2130837523;
        public static final int com_android_settings_com_android_settings_settings = 2130837524;
        public static final int com_android_vending_com_android_vending_assetbrowseractivity = 2130837525;
        public static final int com_android_voicedialer_com_android_voicedialer_voicedialeractivity = 2130837526;
        public static final int com_google_android_apps_maps_com_google_android_maps_mapsactivity = 2130837527;
        public static final int com_google_android_deskclock_com_android_deskclock_alarmclock = 2130837528;
        public static final int com_google_android_voicesearch_com_google_android_voicesearch_recognitionactivity = 2130837529;
        public static final int com_htc_android_worldclock_com_htc_android_worldclock_worldclocktabcontrol = 2130837530;
        public static final int drawer = 2130837531;
        public static final int focused_application_background = 2130837532;
        public static final int folder = 2130837533;
        public static final int folder_open = 2130837534;
        public static final int ic_btn_search = 2130837535;
        public static final int ic_btn_speak_now = 2130837536;
        public static final int ic_launcher_folder = 2130837537;
        public static final int ic_launcher_folder_open = 2130837538;
        public static final int ic_menu_add = 2130837539;
        public static final int ic_menu_gallery = 2130837540;
        public static final int ic_menu_notifications = 2130837541;
        public static final int ic_menu_preferences = 2130837542;
        public static final int ic_menu_search = 2130837543;
        public static final int icon_default = 2130837544;
        public static final int latest_install_app_live_folder = 2130837545;
        public static final int main_dock_theme_shop = 2130837546;
        public static final int menu_panda_theme = 2130837547;
        public static final int nexue_one_style_dock_bg = 2130837548;
        public static final int nexue_one_style_icon_tray_collapse = 2130837549;
        public static final int nexue_one_style_icon_tray_expand = 2130837550;
        public static final int nexue_one_style_land_dock_bg = 2130837551;
        public static final int often_used_live_folder = 2130837552;
        public static final int pandahome_style_handler_bg = 2130837553;
        public static final int pandahome_style_icon_tray_collapse = 2130837554;
        public static final int pandahome_style_icon_tray_expand = 2130837555;
        public static final int pattern_carbon_fiber_dark = 2130837556;
        public static final int placeholder_google = 2130837557;
        public static final int pressed_application_background = 2130837558;
        public static final int preview0 = 2130837559;
        public static final int preview1 = 2130837560;
        public static final int preview2 = 2130837561;
        public static final int preview3 = 2130837562;
        public static final int preview4 = 2130837563;
        public static final int preview5 = 2130837564;
        public static final int s2_bg = 2130837565;
        public static final int search_floater = 2130837566;
        public static final int star = 2130837567;
        public static final int style3_normal = 2130837568;
        public static final int textfield_searchwidget_default = 2130837569;
        public static final int textfield_searchwidget_pressed = 2130837570;
        public static final int textfield_searchwidget_selected = 2130837571;
        public static final int theme_icon = 2130837572;
        public static final int title_bar = 2130837573;
        public static final int wallpaper = 2130837574;
        public static final int widget_clock = 2130837575;
        public static final int widget_search = 2130837576;
    }

    public static final class id {
        public static final int authorView = 2131034113;
        public static final int btn_apply = 2131034120;
        public static final int btn_bar = 2131034119;
        public static final int btn_down_more = 2131034122;
        public static final int btn_theme_market = 2131034121;
        public static final int desc = 2131034117;
        public static final int desc_detail = 2131034118;
        public static final int gallery = 2131034116;
        public static final int hotView = 2131034114;
        public static final int textView1 = 2131034115;
        public static final int titleView = 2131034112;
    }

    public static final class layout {
        public static final int main_layout = 2130903040;
    }

    public static final class string {
        public static final int app_author = 2130968578;
        public static final int app_name = 2130968576;
        public static final int app_title = 2130968577;
        public static final int btn_apply = 2130968582;
        public static final int btn_common_cancel = 2130968586;
        public static final int btn_common_install = 2130968585;
        public static final int btn_market = 2130968583;
        public static final int btn_more = 2130968584;
        public static final int btn_theme_market = 2130968588;
        public static final int drawer_text_color = 2130968604;
        public static final int drawer_transparency = 2130968605;
        public static final int handle_style_parameter = 2130968606;
        public static final int panda_sys_toggle_button_index_x1 = 2130968595;
        public static final int panda_sys_toggle_button_index_x2 = 2130968599;
        public static final int panda_sys_toggle_button_location_x1 = 2130968596;
        public static final int panda_sys_toggle_button_location_x2 = 2130968600;
        public static final int panda_sys_toggle_hotarea_location_x1 = 2130968597;
        public static final int panda_sys_toggle_hotarea_location_x2 = 2130968601;
        public static final int panda_sys_toggle_is_need_bg_x1 = 2130968594;
        public static final int panda_toggle_button_index = 2130968591;
        public static final int panda_toggle_button_location = 2130968592;
        public static final int panda_toggle_hotarea_location = 2130968593;
        public static final int panda_toggle_is_need_bg = 2130968590;
        public static final int panda_toggle_is_need_bg_x2 = 2130968598;
        public static final int pandahome = 2130968610;
        public static final int screen_state_style = 2130968607;
        public static final int sense_ui_special_app = 2130968608;
        public static final int text_back_color = 2130968603;
        public static final int text_color = 2130968602;
        public static final int theme_desc_detail = 2130968581;
        public static final int theme_desc_title = 2130968580;
        public static final int theme_hot = 2130968579;
        public static final int theme_version = 2130968609;
        public static final int tip_hide_icon = 2130968589;
        public static final int tip_no_panda = 2130968587;
    }

    public static final class styleable {
        public static final int[] Gallery = {16842828};
        public static final int Gallery_android_galleryItemBackground = 0;
    }
}
