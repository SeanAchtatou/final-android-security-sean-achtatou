package com.nd.android.pandatheme;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.provider.Settings;
import android.util.Log;
import java.io.File;

public class Util {
    public static void checkMkdirs(String path) {
        try {
            File dirSavePath = new File(path);
            if (!dirSavePath.exists()) {
                dirSavePath.mkdirs();
            } else if (dirSavePath.isFile()) {
                dirSavePath.mkdirs();
            }
        } catch (Exception e) {
            Exception e2 = e;
            e2.printStackTrace();
            Log.e("", "checkMkdirs Exception:" + e2.getMessage());
        }
    }

    public static boolean installApplication(Context ctx, File mainFile) {
        String setting = Settings.System.getString(ctx.getContentResolver(), "install_non_market_apps");
        if (!setting.equals("1")) {
            Settings.System.putString(ctx.getContentResolver(), "install_non_market_apps", "1");
        }
        try {
            Uri data = Uri.fromFile(mainFile);
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setFlags(268435456);
            intent.setDataAndType(data, "application/vnd.android.package-archive");
            ctx.startActivity(intent);
            if (setting.equals("1")) {
                return true;
            }
            Settings.System.putString(ctx.getContentResolver(), "install_non_market_apps", setting);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            if (setting.equals("1")) {
                return true;
            }
            Settings.System.putString(ctx.getContentResolver(), "install_non_market_apps", setting);
            return true;
        } catch (Throwable th) {
            if (!setting.equals("1")) {
                Settings.System.putString(ctx.getContentResolver(), "install_non_market_apps", setting);
            }
            throw th;
        }
    }

    public static boolean isZh(Context mContext) {
        if (mContext.getResources().getConfiguration().locale.getLanguage().equals("zh")) {
            return true;
        }
        return false;
    }
}
