import math
import numpy as np
import time
import pickle
import sklearn
import sys
import os
sys.path.append('/model_NN')

from xml.dom import minidom

resultIA = ""
match_service = 0
match_receiver = 0
match_activity = 0
final = 0
checkIn = []
missingFiles = 0


def APK_analyze(path):
    global final, checkIn, missingFiles
    final = 0
    checkIn = []
    missingFiles = 0
    print("\033[31m____________________________________________________________________________\033[00m")
    input("\033[93m[SYSTEM]\033[00m Press enter to continue, and analyse the permissions of the application...")
    resources_analyze(f'{path}')

    if missingFiles > 5:
        return -1
    print("\033[31m____________________________________________________________________________\033[00m")
    input("\033[93m[SYSTEM]\033[00m Press enter to continue and, analyse the .java files of the application...")
    sources_analyze(f'{path}')

    if resultIA == "Bad":
        final += 40
    if match_service != 0:
        final += 11
    if match_receiver != 0:
        final += 11
    if match_activity != 0:
        final += 12

    print(f'Signature retrieved in the APK : {checkIn}')

    return final

def resources_analyze(resources_folder):
    #try:
    xml_analyze_permissions(f'{resources_folder}/resources')
    xml_analyze_activity(resources_folder,f'{resources_folder}/resources')
    #except:
        #print('\033[31m[ERROR]\033[00m The resources folder has not been found.')

def xml_analyze_activity(root_folder,xml_file):
    global final, missingFiles
    save_Path_ARS = f'{root_folder}/ACTIVITIES_RECEIVERS_SERVICES'
    try:
        os.mkdir(save_Path_ARS)
    except:
        pass

    xml_file = minidom.parse(f'{xml_file}/AndroidManifest.xml')
    use_category = xml_file.getElementsByTagName('category')
    use_action = xml_file.getElementsByTagName('action')
    use_activity_alone = xml_file.getElementsByTagName('activity')
    use_service_alone = xml_file.getElementsByTagName('service')
    use_receiver_alone = xml_file.getElementsByTagName('receiver')

    count_LAUNCHER = -1
    try:
        os.mkdir(f'{save_Path_ARS}/Activities')
    except:
        pass

    for category in use_category:
        javaActivity = None
        try:
            if 'LAUNCHER' in category.attributes['android:name'].value:
                count_LAUNCHER += 1
                parentNode = category.parentNode
                while parentNode.localName == 'intent-filter':
                    parentNode = parentNode.parentNode
                javaActivity = parentNode.attributes['android:name'].value
                try:
                    javaTargetActivity = parentNode.attributes['android:targetActivity'].value
                    javaActivity = javaTargetActivity
                except:
                    pass
        except:
            if 'LAUNCHER' in category.attributes['name'].value:
                count_LAUNCHER += 1
                parentNode = category.parentNode
                while parentNode.localName == 'intent-filter':
                    parentNode = parentNode.parentNode
                javaActivity = parentNode.attributes['name'].value
                try:
                    javaTargetActivity = parentNode.attributes['targetActivity'].value
                    javaActivity = javaTargetActivity
                except:
                    pass

        if javaActivity != None:
            javaClassPath = javaActivity.replace(".","/")
            javaClass = f'{root_folder}/sources/{javaClassPath}.java'
            try:
                os.mkdir(f'{save_Path_ARS}/Activities/Activity_LAUNCH')
            except:
                pass
            resultOS = os.system(f'cat {javaClass} > {save_Path_ARS}/Activities/Activity_LAUNCH/LAUNCHER_ACTIVITY_{parentNode.localName}.txt')
            if resultOS == 256:
                missingFiles += 1

    if count_LAUNCHER > 1:
        print(
            f'\033[31m--[ALERT]\033[00m The APK analyzed seems to have unormal behaviours !')
        final += 16

    for action in use_action:
        javaActivity = None
        try:
            parentNode = action.parentNode
            while parentNode.localName == 'intent-filter':
                parentNode = parentNode.parentNode
            javaActivity = parentNode.attributes['android:name'].value
            try:
                javaTargetActivity = parentNode.attributes['android:targetActivity'].value
                javaActivity = javaTargetActivity
            except:
                pass
        except:
            parentNode = action.parentNode
            while parentNode.localName == 'intent-filter':
                parentNode = parentNode.parentNode
            javaActivity = parentNode.attributes['name'].value
            try:
                javaTargetActivity = parentNode.attributes['targetActivity'].value
                javaActivity = javaTargetActivity
            except:
                pass

        javaClassPath = javaActivity.replace(".", "/")
        javaClass = f'{root_folder}/sources/{javaClassPath}.java'
        try:
            os.mkdir(f'{save_Path_ARS}/Activities/Activities_{parentNode.localName}')
        except:
            pass

        javaClassPathName = javaActivity.replace(".", "_")

        resultOS = os.system(f'cat {javaClass} > {save_Path_ARS}/Activities/Activities_{parentNode.localName}/ACTIVITY_{javaClassPathName}.txt')
        if resultOS == 256:
            missingFiles += 1

        if "BOOT_COMPLETED" in action.attributes["android:name"].value:
            try:
                os.mkdir(f'{root_folder}/ON_BOOT')
            except:
                pass
            resultOS = os.system(f'cat {javaClass} > {root_folder}/ON_BOOT/ACTIVITY_ON_BOOT_{javaClassPathName}.txt')
            if resultOS == 256:
                missingFiles += 1


    try:
        os.mkdir(f'{save_Path_ARS}/Activities_Alone')
    except:
        pass

    for activity in use_activity_alone:
        javaActivity = None
        try:
            parentNode = activity.parentNode
            if parentNode.localName == 'application':
                javaActivity = activity.attributes['android:name'].value
            try:
                javaTargetActivity = activity.attributes['android:targetActivity'].value
                javaActivity = javaTargetActivity
            except:
                pass
        except:
            parentNode = activity.parentNode
            if parentNode.localName == 'application':
                javaActivity = activity.attributes['name'].value
            try:
                javaTargetActivity = activity.attributes['targetActivity'].value
                javaActivity = javaTargetActivity
            except:
                pass

        javaClassPath = javaActivity.replace(".", "/")
        javaClass = f'{root_folder}/sources/{javaClassPath}.java'
        javaClassPathName = javaActivity.replace(".", "_")

        try:
            known_Activities = os.listdir(f'{save_Path_ARS}/Activities/Activities_activity')
            if f'ACTIVITY_{javaClassPathName}.txt' not in known_Activities:

                resultOS = os.system(f'cat {javaClass} > {save_Path_ARS}/Activities_Alone/ACTIVITY_{javaClassPathName}.txt')
                if resultOS == 256:
                    missingFiles += 1

        except:
            resultOS = os.system(f'cat {javaClass} > {save_Path_ARS}/Activities_Alone/ACTIVITY_{javaClassPathName}.txt')
            if resultOS == 256:
                missingFiles += 1


    try:
        os.mkdir(f'{save_Path_ARS}/Service_Alone')
    except:
        pass
    for service in use_service_alone:
        javaActivity = None
        try:
            parentNode = service.parentNode
            if parentNode.localName == 'application':
                javaActivity = service.attributes['android:name'].value
            try:
                javaTargetActivity = service.attributes['android:targetActivity'].value
                javaActivity = javaTargetActivity
            except:
                pass
        except:
            parentNode = service.parentNode
            if parentNode.localName == 'application':
                javaActivity = service.attributes['name'].value
            try:
                javaTargetActivity = service.attributes['targetActivity'].value
                javaActivity = javaTargetActivity
            except:
                pass

        javaClassPath = javaActivity.replace(".", "/")
        javaClass = f'{root_folder}/sources/{javaClassPath}.java'
        javaClassPathName = javaActivity.replace(".", "_")

        try:
            known_Service = os.listdir(f'{save_Path_ARS}/Activities/Activities_service')
            if f'ACTIVITY_{javaClassPathName}.txt' not in known_Service:
                resultOS = os.system(f'cat {javaClass} > {save_Path_ARS}/Service_Alone/SERVICE_{javaClassPathName}.txt')
                if resultOS == 256:
                    missingFiles += 1
        except:
            resultOS = os.system(f'cat {javaClass} > {save_Path_ARS}/Service_Alone/SERVICE_{javaClassPathName}.txt')
            if resultOS == 256:
                missingFiles += 1


    try:
        os.mkdir(f'{save_Path_ARS}/Receiver_Alone')
    except:
        pass
    for receiver in use_receiver_alone:
        javaActivity = None
        try:
            parentNode = receiver.parentNode
            if parentNode.localName == 'application':
                javaActivity = receiver.attributes['android:name'].value
            try:
                javaTargetActivity = receiver.attributes['android:targetActivity'].value
                javaActivity = javaTargetActivity
            except:
                pass
        except:
            parentNode = receiver.parentNode
            if parentNode.localName == 'application':
                javaActivity = receiver.attributes['name'].value
            try:
                javaTargetActivity = receiver.attributes['targetActivity'].value
                javaActivity = javaTargetActivity
            except:
                pass

        javaClassPath = javaActivity.replace(".", "/")
        javaClass = f'{root_folder}/sources/{javaClassPath}.java'
        javaClassPathName = javaActivity.replace(".", "_")

        try:
            known_Receiver = os.listdir(f'{save_Path_ARS}/Activities/Activities_receiver')
            if f'ACTIVITY_{javaClassPathName}.txt' not in known_Receiver:
                resultOS = os.system(f'cat {javaClass} > {save_Path_ARS}/Receiver_Alone/RECEIVER_{javaClassPathName}.txt')
                if resultOS == 256:
                    missingFiles += 1
        except:
            resultOS = os.system(f'cat {javaClass} > {save_Path_ARS}/Receiver_Alone/RECEIVER_{javaClassPathName}.txt')
            if resultOS == 256:
                missingFiles += 1

def xml_analyze_permissions(xml_file):
    global final, resultIA
    special_perm = False
    model = pickle.load(open('model_NN/model_resources_KNN_DANGEROUS.sav', 'rb') )

    xml_file = minidom.parse(f'{xml_file}/AndroidManifest.xml')
    use_permissions = xml_file.getElementsByTagName('uses-permission')
    perm_apk = []
    for permissions in use_permissions:
        try:
            perm_apk.append(permissions.attributes['android:name'].value)
            if not permissions.attributes['android:name'].value.startswith('android'):
                special_perm = True

        except:
            perm_apk.append(permissions.attributes['name'].value)
            if not permissions.attributes['name'].value.startswith('android'):
                special_perm = True

    nn_label = []
    for perm in perm_vector:
        if perm in perm_apk:
            nn_label.append(1)
        else:
            nn_label.append(0)

    resultIA = model.predict([nn_label])
    if resultIA == 'Bad':
        print(f'\033[31m[ALERT]\033[00m The APK analyzed seems to be potentially a \033[41m\033[5mMALWARE\033[00m from the permissions used. \033[31m[ALERT]\033[00m' )
    else:
        print(f'\033[33m[WARNING]\033[00m The APK analyzed seems to be \033[42m\033[5mSAFE\033[00m from the permissions used. \033[33m[WARNING]\033[00m ')

    permIn = []
    for permissions in perm_apk:
        if 'SMS' in permissions and 'SMS' not in permIn:
            permIn.append("SMS")
        if 'AUDIO' in permissions and 'AUDIO' not in permIn:
            permIn.append('AUDIO')
        if 'CAMERA' in permissions and 'CAMERA' not in permIn:
            permIn.append('CAMERA')
        if 'CALL' in permissions and 'CALL' not in permIn:
            permIn.append('CALL')
        if 'BOOT' in permissions and 'BOOT' not in permIn:
            permIn.append('BOOT')
        if 'ACCOUNT' in permissions and 'ACCOUNT' not in permIn:
            permIn.append('ACCOUNT')
        if 'STORAGE' in permissions and 'STORAGE' not in permIn:
            permIn.append('STORAGE')

    if special_perm:
        permIn.append("USE OF SPECIAL OWN PERMISSIONS BY THE APPLICATION ")
        final += 16
        print('\033[31m--[ALERT]\033[00m Use of special and own permissions by the application !')
    print(f"\033[33m[WARNING]\033[00m The application is using the following \033[31m\033[4mDANGEROUS\033[00m permissions on Android : {permIn}")

def sources_analyze(root_folder):
    global match
    match = 0
    save_Path_Source = f'{root_folder}/SOURCES_ANALYSE'
    try:
        os.mkdir(save_Path_Source)
    except:
        pass

    source_LAUNCH(save_Path_Source,f'{root_folder}/ACTIVITIES_RECEIVERS_SERVICES/Activities/Activity_LAUNCH')
    source_ACTIVITIES(save_Path_Source,f'{root_folder}/ACTIVITIES_RECEIVERS_SERVICES/Activities/Activities_activity',f'{root_folder}/ACTIVITIES_RECEIVERS_SERVICES/Activities_alone')
    source_SERVICES(save_Path_Source, f'{root_folder}/ACTIVITIES_RECEIVERS_SERVICES/Activities/Activities_service',f'{root_folder}/ACTIVITIES_RECEIVERS_SERVICES/Service_Alone')
    source_RECEIVERS(save_Path_Source, f'{root_folder}/ACTIVITIES_RECEIVERS_SERVICES/Activities/Activities_receiver',f'{root_folder}/ACTIVITIES_RECEIVERS_SERVICES/Receiver_Alone')
    print(f'\033[33m[ALERT]\033[00m Numbers of matches for \033[36mSERVICES\033[00m : {match_service}')
    print(f'\033[33m[ALERT]\033[00m Numbers of matches for \033[36mRECEIVERS\033[00m : {match_receiver}')
    print(f'\033[33m[ALERT]\033[00m Numbers of matches for \033[36mACTIVITIES\033[00m : {match_activity}')
    print(f'\033[33m[ALERT]\033[00m Numbers of matches : {match_service+match_receiver+match_activity}')

def source_LAUNCH(save_source,launch_path):
    global match, checkIn
    save_Launch_folder= f'{save_source}/Launcher'
    try:
        os.mkdir(save_Launch_folder)
    except:
        pass

    terms_Checks = ['TelehonyManager','setComponentEnabledSetting','HttpURLConnection','SmsManager','recorder','ACTION_CALL','getExternalFilesDirectory','MediaStore','Cipher','JSONObject','getExternalStorageDirectory','Base64.decode','android.intent.action.CALL']

    files = os.listdir(launch_path)
    for i in files:
        for j in terms_Checks:
            result = os.system(f'grep -rHEn {j} {launch_path}/{i}')
            if result != 0:
                pass
            else:
                checkIn.append(j)
                os.system(f'grep -rHEn {j} {launch_path}/{i} > {save_Launch_folder}/Analyse_{j}_{i}.txt')
                match += 1

def source_ACTIVITIES(save_source,activity_path,activity_alone_path):
    global match_activity, checkIn
    match_activity = 0
    save_Activity_folder = f'{save_source}/Activities'
    try:
        os.mkdir(save_Activity_folder)
    except:
        pass

    terms_Checks = ['TelehonyManager', 'setComponentEnabledSetting', 'HttpURLConnection', 'SmsManager', 'recorder', 'Cipher', 'JSONObject', 'getExternalStorageDirectory', 'Base64.decode','android.intent.action.CALL','ACTION_CALL', 'getExternalFilesDirectory', 'MediaStore']

    signature_check = []

    try:
        files = os.listdir(activity_path)
        for i in files:
            for j in terms_Checks:
                result = os.system(f'grep -rHEn {j} {activity_path}/{i}')
                if result != 0:
                    pass
                else:
                    checkIn.append(j)
                    os.system(f'grep -rHEn {j} {activity_path}/{i} > {save_Activity_folder}/Analyse_{j}_{i}.txt')
                    match_activity += 1

    except:
        print("\033[33m--[WARNING]\033[00m No activity found in the application")

    try:
        files = os.listdir(activity_alone_path)
        for i in files:
            for j in terms_Checks:
                result = os.system(f'grep -rHEn {j} {activity_alone_path}/{i}')
                if result != 0:
                    pass
                else:
                    checkIn.append(j)
                    os.system(f'grep -rHEn {j} {activity_alone_path}/{i} > {save_Activity_folder}/Analyse_{j}_{i}.txt')
                    match_activity += 1
    except:
        print("\033[33m--[WARNING]\033[00m No activity alone found in the application")

def source_SERVICES(save_source,service_path,service_alone_path):
    global match_service, checkIn
    match_service = 0
    save_Service_folder = f'{save_source}/Services'
    try:
        os.mkdir(save_Service_folder)
    except:
        pass

    terms_Checks = ['TelehonyManager', 'setComponentEnabledSetting', 'HttpURLConnection', 'SmsManager', 'recorder','Cipher','JSONObject','getExternalStorageDirectory','Base64.decode','android.intent.action.CALL',
                    'ACTION_CALL', 'getExternalFilesDirectory', 'MediaStore']

    signature_check = []

    try:
        files = os.listdir(service_path)
        for i in files:
            for j in terms_Checks:
                result = os.system(f'grep -rHEn {j} {service_path}/{i}')
                if result != 0:
                    pass
                else:
                    checkIn.append(j)
                    os.system(f'grep -rHEn {j} {service_path}/{i} > {save_Service_folder}/Analyse_{j}_{i}.txt')
                    match_service += 1

    except:
        print("\033[33m--[WARNING]\033[00m No services found in the application")

    try:
        files = os.listdir(service_alone_path)
        for i in files:
            for j in terms_Checks:
                result = os.system(f'grep -rHEn {j} {service_alone_path}/{i}')
                if result != 0:
                    pass
                else:
                    checkIn.append(j)
                    os.system(f'grep -rHEn {j} {service_alone_path}/{i} > {save_Service_folder}/Analyse_{j}_{i}.txt')
                    match_service += 1
    except:
        print("--\033[33m[WARNING]\033[00m No services alone found in the application")

def source_RECEIVERS(save_source,receiver_path,receiver_alone_path):
    global match_receiver, checkIn
    match_receiver = 0
    save_Receiver_folder = f'{save_source}/Receivers'
    try:
        os.mkdir(save_Receiver_folder)
    except:
        pass

    terms_Checks = ['TelehonyManager', 'setComponentEnabledSetting', 'HttpURLConnection', 'SmsManager','SmsMessage', 'recorder','Cipher','JSONObject','getExternalStorageDirectory','Base64.decode','android.intent.action.CALL',
                    'ACTION_CALL', 'getExternalFilesDirectory', 'MediaStore']

    try:
        files = os.listdir(receiver_path)
        for i in files:
            for j in terms_Checks:
                result = os.system(f'grep -rHEn {j} {receiver_path}/{i}')
                if result != 0:
                    pass
                else:
                    checkIn.append(j)
                    os.system(f'grep -rHEn {j} {receiver_path}/{i} > {save_Receiver_folder}/Analyse_{j}_{i}.txt')
                    match_receiver += 1
    except:
        print("\033[33m--[WARNING]\033[00m No receivers found in the application")

    try:
        files = os.listdir(receiver_alone_path)
        for i in files:
            for j in terms_Checks:
                result = os.system(f'grep -rHEn {j} {receiver_alone_path}/{i}')
                if result != 0:
                    pass
                else:
                    checkIn.append(j)
                    os.system(f'grep -rHEn {j} {receiver_alone_path}/{i} > {save_Receiver_folder}/Analyse_{j}_{i}.txt')
                    match_receiver += 1
    except:
        print("\033[33m--[WARNING]\033[00m No receivers alone found in the application")


perm_vector = ['android.permission.ACCESS_FINE_LOCATION',
'android.permission.ACCESS_COARSE_LOCATION',
'android.permission.ACCESS_MEDIA_LOCATION',
'android.permission.ACTIVITY_RECOGNITION',
'android.permission.ADD_VOICEMAIL',
'android.permission.ANSWER_PHONE_CALLS',
'android.permission.BODY_SENSORS',
'android.permission.CAMERA',
'android.permission.CALL_PHONE',
'android.permission.GET_ACCOUNTS',
'android.permission.PROCESS_OUTGOING_CALLS',
'android.permission.READ_CONTACTS',
'android.permission.RECEIVE_BOOT_COMPLETED',
'android.permission.RECORD_AUDIO',
'android.permission.READ_PHONE_STATE',
'android.permission.READ_PHONE_NUMBERS',
'android.permission.READ_CALL_LOG',
'android.permission.RECEIVE_SMS',
'android.permission.READ_SMS',
'android.permission.RECEIVE_WAP_PUSH',
'android.permission.RECEIVE_MMS',
'android.permission.READ_EXTERNAL_STORAGE',
'android.permission.READ_CALENDAR',
'android.permission.SEND_SMS',
'android.permission.USE_SIP',
'android.permission.WRITE_CALL_LOG',
'android.permission.WRITE_EXTERNAL_STORAGE',
'android.permission.WRITE_CONTACTS',
'android.permission.WRITE_CALENDAR']




'''perm_vector = ['android.permission.REQUEST_IGNORE_BATTERY_OPTIMIZATIONS',
               'android.permission.READ_EXTERNAL_STORAGE',
               'android.permission.ACCESS_COARSE_LOCATION',
               'android.permission.PROCESS_OUTGOING_CALLS',
               'android.permission.BLUETOOTH',
               'android.permission.SET_ALARM',
               'android.permission.INSTALL_SHORTCUT',
               'android.permission.USE_FINGERPRINT',
               'android.permission.ACCESS_NETWORK_STATE',
               'android.permission.ADD_VOICEMAIL',
               'android.permission.SET_WALLPAPER',
               'android.permission.RECORD_AUDIO',
               'android.permission.MODIFY_AUDIO_SETTINGS',
               'android.permission.RECEIVE_SMS',
               'android.permission.CHANGE_WIFI_STATE',
               'android.permission.ACCESS_MEDIA_LOCATION',
               'android.permission.WRITE_CONTACTS',
               'android.permission.REORDER_TASKS',
               'android.permission.READ_PHONE_NUMBERS',
               'android.permission.ANSWER_PHONE_CALLS',
               'android.permission.DISABLE_KEYGUARD',
               'android.permission.WAKE_LOCK',
               'android.permission.CAMERA',
               'android.permission.READ_SMS',
               'android.permission.WRITE_CALL_LOG',
               'android.permission.UNINSTALL_SHORTCUT',
               'android.permission.ACCESS_FINE_LOCATION',
               'android.permission.CALL_PHONE',
               'android.permission.RECEIVE_WAP_PUSH',
               'android.permission.READ_SYNC_SETTINGS',
               'android.permission.READ_PHONE_STATE',
               'android.permission.SET_WALLPAPER_HINTS',
               'android.permission.ACCESS_LOCATION_EXTRA_COMMANDS',
               'android.permission.WRITE_CALENDAR',
               'android.permission.TRANSMIT_IR',
               'android.permission.GET_ACCOUNTS',
               'android.permission.SEND_SMS',
               'android.permission.READ_CALL_LOG',
               'android.permission.BLUETOOTH_ADMIN',
               'android.permission.BROADCAST_STICKY',
               'android.permission.KILL_BACKGROUND_PROCESSES',
               'android.permission.ACCESS_NOTIFICATION_POLICY',
               'android.permission.WRITE_SYNC_SETTINGS',
               'android.permission.EXPAND_STATUS_BAR',
               'android.permission.CHANGE_NETWORK_STATE',
               'android.permission.GET_PACKAGE_SIZE',
               'android.permission.REQUEST_INSTALL_PACKAGES',
               'android.permission.NFC',
               'android.permission.SET_TIME_ZONE',
               'android.permission.ACTIVITY_RECOGNITION',
               'android.permission.VIBRATE',
               'android.permission.READ_SYNC_STATS',
               'android.permission.BODY_SENSORS',
               'android.permission.CHANGE_WIFI_MULTICAST_STATE',
               'android.permission.RECEIVE_MMS',
               'android.permission.INTERNET',
               'android.permission.USE_SIP',
               'android.permission.READ_CONTACTS',
               'android.permission.RECEIVE_BOOT_COMPLETED',
               'android.permission.WRITE_EXTERNAL_STORAGE',
               'android.permission.READ_CALENDAR',
               'android.permission.ACCESS_WIFI_STATE']'''
