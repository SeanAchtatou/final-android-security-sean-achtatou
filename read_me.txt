This is the root of the project App-MD. If the download has been succesfully achieved you should have 5 folders, 2 pythons files and the read me file.

main_android.py : Main application that have to be launched via the console with 			  the following command : "python3 main_android.py" (python 3 need 		          to be installed, if not type in console -> pip install python3)

apk_analyzer.py : Part of the program which is dedicated to the analysis of the 		  android application, this is composed of the main code and the 			  different analysis which have been achieved on the application.


/Application_APK-Android : Composed of three folders - /Analyze , /Safe , /Unsafe
			   The main important folder is /Analyze, this is where you 				   can import the Android application(s) to be analyzed.

/model_NN : Folder containing all the models used with the Artificial Intelligence,
	    Machine Learning and Deep Learning. Moreover, you are still provided all
	    the malicious and benign applications which have been used to produced
	    those models.



Note : Do not alter any of the folders location, names or code. Doing so, would result in breaking the flow of the application.

Note : All the applications used for the Artificial Intelligence are under a copyrights claimed by the author. Please, do not reuse them unless you have been authorized to do so by the author.
